<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>All_Smoke_Script</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>fbe4d9d8-d22f-4cfa-975a-1ec4a675dc6d</testSuiteGuid>
   <testCaseLink>
      <guid>fdb30473-bc47-43f9-99bb-134e40951408</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/Buyer_Seller-Verify_that_Agent_selection_page_is_displayed_on_click_on_Next_button_in_Congratulations_page</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4a37e946-6cbb-4d9e-b3fd-66d93ff24473</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>17b22488-d76e-4df9-ac89-d097827441ee</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>a7de7278-05ad-4ec9-8334-2237af369bf9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/Buyer_seller_Verify_that_user_can_see_toggle_button_in_the_home_screen_if_user_selects_existing_transaction_from_the_list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>547bb920-f3b7-42eb-ad40-c36f25a34ddb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/Buyer_Seller_Verify_that_user_is_navigating_to_home_page_when_user_taps_on_count_of existing_transaction_button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>840269ff-7942-4863-8645-c652219c0cad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/Buyer_Seller_Verify_the_functionality_of_Cancel_button_functionality_in_all_the_pages_during_onboarding_process</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c161d94a-e2d5-408e-84e3-16495718246e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/Buyer_Verify_the_toggle_option_to_switch_between_the_transactions_in_home_page_and_my_profile_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>768d3823-fc78-422b-b826-0bfc2dc4eae2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Buyer_Seller_Verify_the_components_of_new_registration_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c9c0ebf9-bcc5-4217-b634-0d6cdaa265fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Buyer_Seller_Verify_the_functionality_of_Back_button_in_registration_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1cfe2d4b-28a3-4090-bf86-860a8a369fa5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Buyer_Seller_Verify_the_state_of_Submit_option_after_entering_verification_code_in_the_registration_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9bb9e221-b12e-49e5-93eb-a66599abb291</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_For_Buyer-Verify_that_toggle_to_enable_the_biometric_login_option_is_displayed_in_set_password_screen - Copy</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f99744ef-1cde-4674-83e6-91733eac419a</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a79b920d-affb-411a-87ef-aa4934d164b3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>45b98eeb-ed1d-4d8a-a0e8-980feaa38154</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_For_Buyer-Verify_that_toggle_to_enable_the_biometric_login_option_is_displayed_in_set_password_screen</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>431c9afb-ca82-4d17-b1e2-7d9823e43f50</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1a4a62d1-5c5a-4c55-82d1-c4f0d5d6ea78</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b56168b9-f228-4914-b2cd-4a7163665449</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Test_to_check_the_details_shown_in_the_my_profile_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1690521e-56b7-43ac-8057-60dac457fd50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Test_to_check_the_login_page_entities_from_Registration_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8b2a1c89-d803-44a8-ab98-09217d61ecdb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Test_to_check_the_map_option_is_shown_in_the_My_Profile_details_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8a48c6a-f5d0-4752-a7e5-09df0648602e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Test_to_check_the_navigation_from_my_profile_page_to_home</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f52c460-0486-45d1-b18e-e979a788f72f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_To_verify_contents_displayed_in_mortgage_provider_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0bdc5bd7-83c8-40ff-9fbe-e9657a90c9d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_To_verify_Homepage_validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3e943c1-e888-407e-acb6-f51c91c79210</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_To_verify_task_category_contents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>79aa7fe0-b2a8-4c2b-be99-64a61dc699fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify Consumer is able to see the Congratulations on listing your home with Coldwell Banker todo task on successful registration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d73838f-7835-4701-be6e-b89f8245ed93</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify My Journey button is present</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cad2a240-5c07-42b5-b8fa-f88b7357d8e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify the item with Heading-Get to know your Selling Journey</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>82b82d35-a56f-49ce-a221-58a55e803a8c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify _that _customer_ should_ be _able _to_ delete_ the_ recommended_ service_ provider_ information</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d34b0d3f-c013-4e04-b3ff-6081cd1fecd2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify _the_ Recommended _Service_ Provider_ under _My _Teams</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>03b0c4d3-f514-4c88-8e8d-17fb4d840ff6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_About_app_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6e6f8db-531a-467e-9feb-b0b2250053e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_Buyer_is_able_to_Login_to_app_using_the_Log_In_link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e00adb1-29da-4162-af68-c0bc0be9b036</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_Buyer_is_able_to_see_the_updated_landing_page_on_App_launch</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0aa3885-508c-4ace-b0e1-c99ab65e4369</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_buyer_is_redirected_to_tell_us_about_you_screen_on_click_of_ok_button - Copy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f6987beb-be42-4ed2-bf7e-fb7b6c8e5046</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_buyer_is_redirected_to_tell_us_about_you_screen_on_click_of_ok_button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44ae0e5d-1117-4063-8774-7264f9ddaca5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_buyer_is_redirected_to_the_profile_validation_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4236f274-48e7-4c61-a70e-56f3c92cb16a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_change_password_in_my_profile_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ef86c0f-63be-4ab2-84f7-422c04fa84ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_consumer_is_able_to_view_and_enter_the_feedback_in_the_feedback_form-new_user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c4b4829-2d0d-4dc2-9d14-1a3a16bac540</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_consumer_is_able_to_view_and_enter_the_feedback_in_the_feedback_form-Registered_user</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>833ef39f-ee7d-4543-a9ae-a561617c793e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_consumer_is_able_to_view_the_ basic_functionalities_ of_the_different_features in _the_app_by_clicking_the_guide_icon_on_the_top</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bfefab85-9af4-4134-bb65-17074f6bbcf9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_Create_Team_option_in_My_Profile_page_for_adding_team_member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>822223d3-c143-46d2-bae0-801e01ee4ceb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_logout_option_for_seller_flow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c00d0ca-0f91-40aa-b0a0-91598d2af7d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_Mark_as_done_in_the_to_do_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6db5a9b1-7954-4a24-bafe-5adf1acdd4a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_navigation_to_NewPassword_Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8d259d1-3598-4ac5-9593-1c0acacd3641</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_Privacy_policy_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a46f7327-b359-4b2a-a66e-5de6fa60d3f5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_Rate_us_option_in_seller_flow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abd89e4e-11e6-4daf-863f-411c496e60a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_Seller_is_able_to_Login_to_app_using_the_Log_In_link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d60c226-3db9-4f7d-84e2-c8191ad0fc6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_Seller_is_able_to_see_the_updated_landing_page_on_App_launch</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e6c0147-1916-4311-aed6-b97ba1f29d48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_seller_is_able_to_view_the_screen_tips_of_the_Listing_Agreement_section_by_clicking_the_guide_icon_on_the_top</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>87ab1b04-c857-47af-86f5-077457552693</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_seller_is_able_to_view_the_screen_tips_of_the_My_Journey_button_by_clicking_the_guide_icon_on_the_top</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c06e4c7d-a831-45f9-b70c-75048908833f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_Submit_your_initial_earnest_money_(payment) _title_and _description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fecbda35-5d2a-4a8f-b04b-2e3b876d69c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_Terms_of_Use_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22836b9b-a4a8-4d6a-9b5b-b8a7ab7c6439</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_that_Show_Need_Review_option_should_be_displayed_right_near_to_the_My_Docs_title</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cfd26153-7554-443d-b01b-195cc6bb1a01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_that_the_seller_is_redirected_to_My profile_page_on_clicking_Next_option_Seller_flow - Copy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5441855f-337a-4c2c-8a7b-baefcc84a4ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_that_the_seller_is_redirected_to_My profile_page_on_clicking_Next_option_Seller_flow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>259a7e80-a6a8-4fab-9322-e58717867913</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_that_the_seller_is_redirected_to_My_profile_page_on_clicking_Skip_option_and_nothing_is_displayed_under_personal_info - Copy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dcae120b-4f55-4668-aa4f-dfdad4c146a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_that_the_seller_is_redirected_to_My_profile_page_on_clicking_Skip_option_and_nothing_is_displayed_under_personal_info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c2d2b220-daf3-40ca-8248-fee0908eec43</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_Agent_selection_page_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eb79fa6f-30fe-4c83-91c0-4b7f400995b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_appearance_of_Submit_your_initial_earnest_money</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5a3f774c-4571-4d39-a6b5-269d7be349e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_Back_button_in_top_left-corner_in_Notifications_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>30bc1a2c-b9be-4ebf-bd2d-bcf68a123e76</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_bottom_navigation_entities</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9cb1d670-d839-4c51-9781-1370375a2ac6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_Cancel in the delete popup for recommended service provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8977ef97-f1de-47b8-9fa3-b009756e7650</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_contact_details_of_the_agent</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>018e52f8-8838-4f70-91b2-cf8ef1ebba74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_contents_in_Tell_us_about_you_page - Copy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>02f989e7-9fc8-4f09-a2f9-b312816426a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_contents_in_Tell_us_about_you_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec34d7d2-59c7-468e-97ea-1055ea0ac6fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_Forgot_email_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1562a3a-237e-4db5-a2ac-2333c43f1766</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_home_page_toggle_value_with_the_value_as_selected_buyer_transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e60133fd-336b-49e2-85d5-0499b794755f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_home_screen_entities</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01d6420c-fc0b-489a-826f-b01675987cdf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_Know_Why_content_in_My_teams_tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fdad31dc-4c15-4b38-a539-08c0dd689bd0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_logout_functionality_of_C2C_app</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e52572b5-65ef-4e88-a6de-6a0e6f6859b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_My_Journey_button_content_in_Homepage_top_right_corner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0124c89d-3b6e-47e9-8eb8-e3d97ec2ed79</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_Privacy_policy_page_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>11dd6db7-28d0-434e-bc1e-7dffc8a1c29d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_retrieve_email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1cbdc28-d1fa-48af-aaae-350e36eab801</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_screen_which_lists_out_all_property_addresses_with_buying_indicator_when_all_are_buyer_transactions</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1b464a07-f714-457e-b887-1ee7f1bcd5ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_sections_ under_My Docs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>531cb010-45a7-44e0-a689-32a4559995b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_sections_in_Profile_setup_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f628e8f-a451-42a4-be60-196f7b6b15d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_sequence_in_Todo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa05d595-c069-43a1-946a-8f26e6480b80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_settings_options_under_profile_details_in _My Profile_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58ce3baa-2c8a-4899-9acf-103c4eb62a77</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_top_navigation_entities</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75af2aaf-9f5f-4a96-bd63-adefee059aad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C2SC_Verify_the_weblink_for_recommended_service_provider_on_Team_members_contact_details_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d2baa532-6db0-4641-977d-822e657e9d6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/C601916_C2SC_SellerFlow_Verify_that_customer_is_abl_to_see_Listing_Documents_section_under_My_Docs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38edf71a-ff52-4a32-8b4b-b51138fd0ecb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/For_Buyer_Email_number_Verify_if_user_enter_the_correct_verification_code</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>09a3f2e2-d6a9-43b4-ac8e-b43b649c45a6</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>441df69a-01e1-4e7b-856b-f9edf924556d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>56877c50-5333-48a6-85b3-e4f96abba98c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/For_Buyer_Phone_number_Verify_if_user_enter_the_correct_verification_code</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>1da39429-257e-4cf2-b916-dda8f9647ef5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a5b62ad6-1848-4b4f-b27c-38fb6638015a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>446f0275-2a69-413f-9c5b-f4e57c701c61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/For_Seller_Email_number_Verify_if_user_enter_the_correct_verification_code</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>338282e1-86f1-4b47-80b5-14f19da8f2f7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b82cadd7-89aa-4328-ac26-96337172f50c</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c24c315f-fc9f-40a5-b753-8d1f36ab1222</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/For_Seller_Phone_number_Verify_if_user_enter_the_correct_verification_code</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b2f741b5-f555-4c2f-a085-1234a1b35660</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6702ec80-e1ec-43f6-a86f-18dc62f68c4a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e766dafa-248b-4f54-bf88-bdcb536ce1aa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/To verify the team member details page if Attorney is selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7f97deef-1ac1-4050-8370-56792d764567</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/To verify the team member details page if Inspection is selected from dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a695628c-62a6-4b53-9ca8-d3e54bffcaa3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/To verify the team member details page if Other is selected in dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20d233c2-408f-4d5a-8220-7c8d41822615</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/To verify the team member details page if service provider is GRA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5d2e347-897b-48bb-b5f1-03fe454144d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/To_verify_content_of_Property_Insight_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bca46c5f-427e-4c07-ba98-7f1c64d0de63</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/Verify_the_details_of_property_are_displayed_under_the_address_Under_contract_option_in My profile page_SellerFlow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d372f1ff-0e0a-4416-b1cf-a8c9addff415</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/Verify_the_forgot_password_screen_on_entering_registered_email_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2168fa3d-b4c0-4278-9994-4837c00082a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/Verify_the_map_view_option_My_purchase_option_in Home page_SellerFlow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c5dc7416-2a7e-421d-8cb7-eea7c2c6e499</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Smoke_TestScripts/Verify_the_map_view_option_Under contract option_in My profile page_SellerFlow</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
