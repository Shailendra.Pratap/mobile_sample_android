<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_MyJourney</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>346a5050-13f4-418d-873e-0c87c6def150</testSuiteGuid>
   <testCaseLink>
      <guid>2eab7bb8-793a-4a6d-9ea1-5b12be70c856</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_MyJourney/C2SC_Verify_the_back_button_in_My_Journe_page_in_Homepage_top_right_corner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6ad187d-4c86-4ef9-adc7-9d63c9a3752a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_MyJourney/C2SC_Verify_the_icons_in_My_Journey_page_in_Homepage_top_right_corner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18f903df-3791-431e-8c52-603e14fe7411</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_MyJourney/C2SC_Verify_the_My_Journey_button_content_in_Homepage_top_right_corner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d93c58b-3a48-4baa-bfb1-dc869076b06e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_MyJourney/MyJouney</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
