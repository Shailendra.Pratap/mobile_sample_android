<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_ConsumerFinancialScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ff7afa62-0a92-4da0-b60a-53360dc67ae1</testSuiteGuid>
   <testCaseLink>
      <guid>1e0170a7-4094-4112-8e95-c5d367b4391c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerFinancialsScreen/Verify_the_ carousal_ of_ property_ pics_are_ displayed_My purchase option_in Home page_SellerFlow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8e11c06-ace4-461f-86a1-d30bd633cea5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerFinancialsScreen/Verify_the_map_view_option_Under contract option_in My profile page_SellerFlow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5bc1f2fc-0f67-462d-931a-a2f45c1ac71d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerFinancialsScreen/Verify_the_back_button_is_displayed_on_the_top_left corner_My_purchase_option_in Home page_SellerFlow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a30a2ff0-6d36-4910-b8f9-61b01674fc24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerFinancialsScreen/Verify_the_back_buttondisplays_ on _the top _left corner_Under contract option_in My profile page_SellerFlow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0261f364-b7ae-46a1-a126-1cd8eb2c0b6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerFinancialsScreen/Verify_the_carousal_of_property_pics_are_ displayed_Under_contract_option_in My profile page_SellerFlow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7057c9ff-e48a-4c6a-b54f-4a8e445b6af6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerFinancialsScreen/Verify_the_details_of_property_are_displayed_under_the address_My_purchase_option_in Home page_SellerFlow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f339ac98-307e-40dc-b9a5-6f5d7cb8fb28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerFinancialsScreen/Verify_the_details_of_property_are_displayed_under_the_address_Under_contract_option_in My profile page_SellerFlow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18ee8962-35e1-4521-87e1-0b28f990458d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerFinancialsScreen/Verify_the_map_view_option_My_purchase_option_in Home page_SellerFlow</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
