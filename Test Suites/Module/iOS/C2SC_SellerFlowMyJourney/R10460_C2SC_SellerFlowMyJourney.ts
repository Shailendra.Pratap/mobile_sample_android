<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_SellerFlowMyJourney</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>5ff21485-b73d-43d1-acae-a9d26d7310f6</testSuiteGuid>
   <testCaseLink>
      <guid>43bfa3ea-0046-4c63-b99c-5b93f93f59a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_SellerFlowMyJourney/C2SC_Verify My Journey button is present</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1edf9f6b-7715-4c23-8334-4f6c1fdd8a0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_SellerFlowMyJourney/C2SC_Verify the heading of the My Journey page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d7b6c96e-c2b6-48a8-aa01-50750bdebca6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_SellerFlowMyJourney/C2SC_Verify the item with Heading-Confirm your title has been ordered</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0653c316-207c-492d-bb8f-c527c840636e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_SellerFlowMyJourney/C2SC_Verify the item with Heading-Get to know your Selling Journey</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6ba32d6-0a63-4efd-84a1-0f387d7a1062</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_SellerFlowMyJourney/C2SC_Verify the item with Heading-Schedule Walkthrough</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5e0d70f-a17d-4446-b3f3-592006837bc0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_SellerFlowMyJourney/C2SC_Verify user is able to navigate back by clicking on the Back arrow in My Journey page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
