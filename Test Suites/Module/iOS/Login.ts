<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>80a8ff13-2844-45db-8589-579252cb0470</testSuiteGuid>
   <testCaseLink>
      <guid>5c1da88c-8d1d-4fa9-84f3-a91d15e523a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_TermUsePrivacy_Verify_the_terms_and_conditions_page_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ec9a4982-d3cb-4528-997a-22739fe42f32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Test_to_check_the_login_page_entities_from_Registration_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e8c6fc5-edd3-464b-871a-de6f067f0e91</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Test_to_check_whether_buyer_is_redirected_back_to_registration_page_on_click_of_close_button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0e23a1a4-dacb-4f62-a22d-c5cfd54085d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_To_verify_login_screen_display_upon_relaunching_app_when_consumer_completed_registration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5800797b-1683-45f9-a7cc-455b5532b10a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify the message when user tries to Reset the password with correct email ID</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9080716-ee3e-4a56-b928-6d2964f4f084</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify the submit code functionality</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c874a9ee-c844-4a7d-8faf-2cc95cb0c7ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_the_alert_message_is_displayed_on_clicking_Dont_have_an_auth_code_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ed432eb-37d4-45b9-ac4f-5253a9de0731</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_the_error_message_displayed_on_entering_an_invalid_code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a0b55bb6-b13f-45ef-a278-032c3740fe49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_the_error_message_for_invalid_code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe5f90ef-5ce5-47f8-8747-d83f8d24ec16</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_the_Privacy_policy_page_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe8fdfaa-7aee-46fa-8f84-01e0c9a357d6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_the_user_able_to_enter_the_mandatory_details_in_Authentication_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9878a5ad-5bae-4c1e-bdc8-55adf45e519b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_the_user_is_redirected_back_to_Authentication_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8eb4616a-9855-42d6-82a8-34b86d68cd29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_the_user_is_redirected_back_to_authentication_page_from_privacy_policy_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9140a793-4015-48b1-bf92-f82ecd154665</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_user_able_to_view_profile_validation_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13db406a-612b-4da5-9840-2c1558277fc3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_user_able_to_view_terms_and_conditions_from_Profile_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d1c6dbd-be20-47fe-8cad-b04e95941890</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_user_able_to_view_the_Authentication_page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
