<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_TodoScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>d203d292-b89e-4302-923f-cb28accaa538</testSuiteGuid>
   <testCaseLink>
      <guid>de71c100-84ad-48eb-8fc7-33b6956536f8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_SellerFlow_Verify_that_Schedule_Home_Showing_task_is_present_in_To_do_tab_in_home_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8be31396-32c1-4332-bcf5-fbbc259f14fe</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_SellerFlow_Verify_the_input_fields_present_for_schedule_home_showing_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13524d1a-69d1-4fd4-a3ba-a31e9015ada2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_To_validate_the_View_task_displayed_on_clicking_the_self_assigned_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6cfda6f9-f026-430f-a1c2-e1bd7734cae6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_To_verify_clicking_delete_option_for_a_self_assigned_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ba24bf88-abf0-4bd3-9d47-776eba5f331b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_To_verify_clicking_edit_option_for_a_self_assigned_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d6380488-25eb-45f8-a9a7-ea53c6f53a10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_To_verify_consumer_can_enter_data_in_company_field_on_Schedule_Inspection_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>985b4caa-73e1-4f95-bfbd-92b29a56f1ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_To_verify_marking_a_self_assigned_task_as_done</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f65c1dbd-88a1-4782-bd03-4ff89409aa35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify Consumer is able to see the Congratulations on listing your home with Coldwell Banker todo task on successful registration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81676666-516d-4b22-bb0c-103bec8910c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify Consumer is able to view the content of to-do task and perform events</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fddc0007-4fdd-49bc-ac0d-7cee8bf0b1bf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify that schedule closing to do task, then the date will be blank if consumer has not completed task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e0d27d19-7d7d-46f1-9db7-5cf4cf39dc99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify the closing details displayed in the homepage on closing day when schedule closing is not done and title company_is_not_chosen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac901191-813e-49bd-8ec4-191bb290bd36</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify the closing details that is displayed in homepage on closing day when schedule closing is done and title company is chosen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7429e448-92a8-4ecf-a54d-9a042834cf78</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify the Closing Reschedule task in Timeline got updated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9aba945a-78a6-4219-ba3f-68c316c5987b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify the fields in reschedule closing screen when user selects Rescheduled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b05bb96d-3671-47a4-8c3a-abfc3dc87038</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify the options under What went wrong</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a7d33ea6-f79e-473b-b62c-3464238acfdb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify the rescheduled closing date is updated at the top of home screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90b636c8-43a1-47b9-8fc5-b1b52c26680f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify to-do list is not viewed until the flow is completed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>97ec7b61-7d9b-4093-a35e-f6b3cd6b18ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_all_fields_are_present_on_Add_Item_screen_as_per_design</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d06c6bd9-5419-4ea7-a55a-5fa495df413a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_buyer_able_to_add_the_task_when_enter_data_all_input_fields</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c3c0166-e706-4798-9b75-a9ffd7f6295b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_Closing_Documents_Available_description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>936a28b6-05fe-4d9d-b04a-c0d728c2eeee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_discarding_the_self_assigned_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e5e5907e-7bc4-4c0a-b2e7-72d6d7bbd31f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_display_of_error_message_when_required_fields_not_added</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ffc2489-79fd-45a8-b09f-4b43e71e1b64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_marking_a_self_assigned_task_as_done</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cf19f10-6fe6-4f1d-9b6a-f936cc43c856</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_Obtain_HomeOwners_Insurance_title_and _description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bcc0a887-c361-41db-8ffe-97e4b68e8ebd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_on_click_of_close_icon_when_no_data_is_entered_buyer_should_navigate_to_My_Home_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d4e4d46-473a-47f1-830b-3b3c86ace766</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_pop_up_message_is_displayed_when_buyer_filled_form_and_click_close or back_icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>406f0948-35dc-404d-9b78-701ccbffbb90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_saving_the_self_assigned_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d619f9f-3e30-44e3-868a-afa26f65066a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_Schedule_Closing(Calender) _title_and _description</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f9a487da-8515-4a9e-88af-3d1b76925ea3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>980c2fd6-e09a-45fc-bf94-283c548b2ccf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_Schedule_Inspection(Calender) _title_and _description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>54a41b08-2303-4eae-94b9-1dd00630f385</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_Schedule_Inspection_Time_picker</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>89361bdc-333a-443e-a44e-5807fca1fd73</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_SellerFlow_error_messages_displayed_for_Date_Start_time_and _nd_time_fields</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>591b825a-292c-463c-9eb4-2e3de17ae3ca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_Submit_your_initial_earnest_money_(payment) _title_and _description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>74605e61-6482-425c-ad8a-a625aa3ab758</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_the_appearance_of_Schedule_Walk_Through_task_in_To-Do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>635c0ea8-fed9-4a54-a6c2-087e6867c112</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_the_fields_shown_for_system_task_Schedule_Walk_Through_in_To-Do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>38c6611e-0ddb-4501-98bc-122ef980a417</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_the_Schedule_Walk_though_form_which_contains_the_required_fields_and_respective_error_messages</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ece40799-5112-4d22-9fa9-8801eb909907</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_the_status_of_task_on_click_of_Add_in_the_Schedule_Walk_though_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b758b7d6-a90d-4b96-94f8-5e7e49297c72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_Title_commitment_form_received_(pdf)_title_and_description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31c341d6-5b58-4b3a-af7a-fc845c6967a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_when_Add_Task_form_is_saved,_that_task_should_be_listed_under_To_Do_tab_on_Home_Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fecfc6de-0c60-4470-9a2d-102ca0beace1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_when_buyer_clicks_Cancel_button,_buyer_should_stay_in_Add_Item_screen_and_edited_fields_remain_intact</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43c757b3-844f-40a9-9b09-7d211c267809</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_when_buyer_clicks_Discard_button_buyer_should_be_navigated_to_Home_page_and_edited_task_is_discarded</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>136cff24-d7d6-4a9c-8dd9-702c6e32ae7e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_whether_the_user_is_able_to_view_the_header_and_number_of_to_do_tasks</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5db2f12-d31e-4b63-9526-11e828b25325</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C541196_C2SC_Verify_the_fields_and_respective_error_messages_in_schedule_closing_form</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af22f764-2188-423b-bb81-49a0db67ab1b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>f8af6d45-4a80-46f2-8277-e247ab61a9e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C541197_C2SC_Verify_the_status_of_task_on_click_of_schedule</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>36644583-3dff-402c-9d2c-8246a656332a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>39f66184-3a80-46ca-8e2c-fa7f6405dee2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C569843_C2SC_To verify self added GRA contact name display in team dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0fd8e6d7-66dd-4beb-83ba-a33a7b0c40f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C569844_C2SC_To_verify_consumer_can_edit_Team_member_on_editing_ToDo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ba6b13ed-a636-4634-9457-bf8b10e6d306</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/SellerFlow_Verify_that_Provide_Wire_Instructions_to_Title_Company_task_is_not_available_4days_prior_to_closing_date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47a84a66-e95e-432e-8c8d-e67d7e97e6b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/SellerFlow_Verify_that_Provide_Wire_Instructions_to_Title_Company_task_is_not_available_for_buyer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d714b5c-a228-4121-b504-c8bf6db4bf7d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/SellerFlow_Verify_the_contents_of_Provide_Wire_Instructions_to_Title_Company_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2d39dc3-be07-49f5-83d9-c6c737aa7768</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/SellerFlow_Verify_the_functionality_after_clicking_on_Confirm_button_in_the_Provide_Wire_Instructions_to _Title_Company</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f8e67c20-c051-457d-b8cf-eb798792e6a9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/SellerFlow_Verify_the_Title_Due_and_Description_for_the_task_Provide_Wire_Instructions_to_Title_Company_in_ToDo_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ca0d44f9-288f-424e-a292-2547f662913d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_My_Docs/To_verify_the_addition_of_counters_to_contract_section_in_My_Docs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1b15f98a-29b6-423b-85c8-30d0294e8ab4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/Verify_the_contents_of_Review_ListingAgreement_screen_displayed_SellerFlow</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
