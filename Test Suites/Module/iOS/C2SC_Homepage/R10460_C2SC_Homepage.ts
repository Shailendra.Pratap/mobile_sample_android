<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_Homepage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>70fbb3da-207e-48f7-8e5c-24f7c4cb1dd0</testSuiteGuid>
   <testCaseLink>
      <guid>f0e1d7f4-4440-4cda-9cbf-ed79746896ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Buyer_Verify_My_Teams_page and mortgage_details_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75064918-2ae0-4d5e-b2ae-6ad90e42cca9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Buyer_Verify_the_Agent_details_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd3039f1-202d-4708-b08e-c06c8345d46c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Buyer_Verify_the_closing_status_in_top_of_home_page_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6dee4db0-297b-4063-bfd4-4c1401d7b165</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Buyer_Verify_the_My_Docs_page_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8342c4ac-565a-4ba9-b1b5-5bc02c569b2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Buyer_Verify_the_toggle_option_to_switch_between_the_transactions_in_home_page_and_my_profile_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b325545d-273c-4b59-8f9c-4d6ce3cfa6a2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Buyer_Verify_the_To_Do List_and_Timeline_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c44570ca-6ac7-4d93-88e7-9a8f3e72dd35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Buyer_Seller_Verify_the_toggle_option_to_switch_between_the_transactions_in_homepage_and_my_profile_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3163372-092d-456c-b24e-7ebfa6c96c95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_To_verify_display_of_closing_date_and_number_of_days_to_closing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5161525-24b8-44e5-b311-c8746bdadded</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_To_verify_display_of_Closing_Day_in_place_of_estimated_difference</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5acb0f1e-efd4-44f0-8c25-552f02a35b72</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_To_verify_display_of_on_track_Icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5333c411-4295-4a82-bb54-30c50f0b9057</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_To_verify_Homepage_validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aceb6261-cd1a-4bce-9dc8-1febda4dc634</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_To_verify_task_category_contents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f8024f53-421e-4347-bda4-554ab4e1a2e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify the heading on top of My Docs page got changed from C2C to HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>42edea85-50bf-4e3a-88ef-5a3887902936</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_consumer_is_able_to_see_Transaction_Completed_screen_once_the_Transaction_is_closed_successfully</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af3b5b2f-9689-4490-b633-50950fec26a7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f6631b8a-dce9-4670-bdae-ed8c723d77f1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6c34a4ce-c626-4e25-9a53-c0e29fddc54f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_consumer_is_able_to_use_all_the_features_as_it_is_after_closing_the_Transaction_successfully</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d70a1be3-cdae-482b-857e-999154ef0604</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5549cdd0-3638-4fda-bdc4-995227772741</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e328c276-d3bf-49db-b210-b2c38cc7834b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_Mark as_done_under_submit_your_initial_earnest_money</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6cc5c578-3ad5-4154-bfff-61e41c138d33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_Mark_as_done_in_the_to_do_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>242ae02a-bd7a-46c2-b820-263310b99ea6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_added_notes_are_displayed_for_second_time_when_we_open_task_if_consumer_opted_for_Save_Continue_Later</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a6432d3-b0b3-45e0-81d3-78251475726e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_added_set_of_notes_are_adding_as_a_list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6ffedc75-e7a2-4c1b-ad53-98c4334e381c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_Note_to_Buyer_is_appearing_in_to_do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1ec2215-df1a-46ac-86c6-3e3963939bb2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_Note_to_Buyer_task_is_moving_to_Timeline_when_we_mark_as_Done</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fb6244d2-eebc-4fda-8785-44fd9c7d86c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_no_document_is_available_for_the_to_do_task_when_a_no_service_provider_is_selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae5f3edf-16f6-4eff-b256-dd3e14484edf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_no_document_is_available_for_the_to_do_task_when_a_recommended_service_provider_is_selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39f88ec7-e722-4efc-9854-b98628e116be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_no_document_is_available_for_the_to_do_task_when_a_self_preferred_service_provider_is_selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da2501fa-1369-4e71-a406-f3cb33c0daf4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_title_Schedule_Open_House_is_displayed_in_To_Do_page</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af3b5b2f-9689-4490-b633-50950fec26a7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f6631b8a-dce9-4670-bdae-ed8c723d77f1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b3dfd95f-93a8-4027-97db-79b86149225b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_appearance_of_obtain_homeowners_insurance_and_closing_documents_in_to_do_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c5543ce1-13f3-43b4-b412-6a85c5a5d91d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_appearance_of_Submit_your_initial_earnest_money</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e639284-a139-49ca-881a-6983ed2f01d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_close_icon_X_in_schedule_inspection_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28d01c4c-86ae-4c11-b987-3591cae158cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Date_field_in_the_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff6ca5fc-e7dd-49e2-80c7-a0996ac844e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Description_text_Note_to_Buyer_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b96d1f1e-940f-42e6-bdff-643e3bef7524</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Due_days_for_Note_to_Buyer_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4390bf8a-01b3-42c5-9e2d-0a66bbec5509</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_error_message_displayed_for_date_field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78613af9-4969-4cb6-b39f-c361fff4454e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_error_message_displayed_for_End_Time_field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a967e815-421b-460d-b26c-abf2b8e137b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_error_message_displayed_for_Start_Time_field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88e88254-89cd-45ef-9155-676609570724</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_error_message_for_Time_field in_schedule_inspection_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f9ef458-3529-4fb7-808a-bfb62452113d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_fields_shown_for_closing_documents_task_in_to_do</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af3b5b2f-9689-4490-b633-50950fec26a7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f6631b8a-dce9-4670-bdae-ed8c723d77f1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>b87d12e5-4c38-4272-8dc5-8dbe3e585778</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_fields_shown_for_obtain_homeowners_insurance_task_in_to_do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f7d9737-9a21-4e87-b740-3eb1ed140457</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_fields_shown_for_Title_commitment_form_task_in_to_do</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af3b5b2f-9689-4490-b633-50950fec26a7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f6631b8a-dce9-4670-bdae-ed8c723d77f1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d585fa53-85e5-48cf-be86-c272456b4503</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_Home_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a269b07-d9f7-47e6-91a4-48298abc54fb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_My_Agent_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d5dddfca-1d41-473d-b377-d58bb677b26a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_My_Profile_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1c4e4a7e-ea07-458b-a0b8-f9a3038ccf50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_My_Team_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c935eadb-4f38-463c-bb7e-e236ec3dfd75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Inspection_Company_field_in_the_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2203cf90-8ad6-4c3f-a447-d8caee3770e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_mark_as_done_button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6f0ece3a-c47c-4078-b24a-699af94800f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_options_under_submit_your_initial_earnest_money</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85a21f36-edc0-4867-a566-3a8a1767ba03</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Save_and_Continue_Later_button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db26f377-5144-4864-8f4b-e686cc129917</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Schedule_Inspection_on_clicking_of_done</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db66244d-6cc9-4a96-8707-7b76f661ec49</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_sequence_in_Todo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>81443890-872d-4a80-b4e6-925da63bb133</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_verify_the_status_on_click_of_Obtain_Homeowners_insurance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a47a2f2e-0167-4a8e-8835-4c6f8feed9f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_SubText_for_text_Note_to_Buyer_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cfcca0bb-457e-441e-8abd-20d17b926092</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Time_field_in_the_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60baaf10-8048-40c7-bfc0-0a77cc0ac4f3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Title_description_due_days_for_Submit_your_initial_earnest_money_task_in_todo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45e3c2ca-ee94-4df7-bfb5-b7e90a233935</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_to-do_page_and_timeline_after_adding_Schedule_Open_House_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bad55ee8-5e7b-41a7-9943-a451d684922e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Wire_Funds_for_closing_pdf_task_in_to_do</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af3b5b2f-9689-4490-b633-50950fec26a7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f6631b8a-dce9-4670-bdae-ed8c723d77f1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d44e9121-5c70-4a91-85f2-08745e98a582</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Seller_Verify_My_Teams_page_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>230f63e1-06c9-4482-ae76-7396f1daf976</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Seller_Verify_the_closing_status_on_top_of _home page_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5eb7e656-d9a9-474d-a00b-582b638042d3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Seller_Verify_the_My_Docs_page_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0fd8db7a-8ea7-45fe-aa07-2c9f99bb78dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Seller_Verify_the_To_Do_List_and_Timeline_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
