<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_TellUsLittleAboutYouScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>a238b41a-6b8a-4dd1-82c0-21168d242fe6</testSuiteGuid>
   <testCaseLink>
      <guid>90a446f2-e923-4c96-ae83-6fbe2cbceb57</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TellUsLittleAboutYouScreen/C2SC_Verify_that_the_Next_option_is_in_disable_state_when_no_options_are_selected_in_Tell_us_little_about_you_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>91696f65-c870-41cc-8daf-05fa0553f4c8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TellUsLittleAboutYouScreen/C2SC_Verify_that_the_seller_is_redirected_to_My profile_page_on_clicking_Next_option_Seller_flow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c34e1bbb-0d40-4d65-8e02-1e44546d653b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TellUsLittleAboutYouScreen/C2SC_Verify_that_the_seller_is_redirected_to_My_profile_page_on_clicking_Skip_option_and_nothing_is_displayed_under_personal_info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73992768-34be-4dd6-aae8-79f62e8e3874</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TellUsLittleAboutYouScreen/C2SC_Verify_that_the_user_navigates_to_homepage_by_clicking_Next_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e40cbae-e2c2-4ae5-b239-2c74d493eb20</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TellUsLittleAboutYouScreen/C2SC_Verify_that_the_user_navigates_to_homepage_by_clicking_skip_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac34a409-b42b-46a1-81d2-435756e4c54a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TellUsLittleAboutYouScreen/C2SC_Verify_the_contents_in_Tell_us_about_you_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc4afd3b-d238-4b2e-860f-3013b2b2e301</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TellUsLittleAboutYouScreen/C2SC_Verify_the_contents_of_Tell_us_a_little_about_you_page_seller_flow</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
