<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_Address_Map_Details</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>8878f93f-f409-4eb1-ac74-ec23b1652b0f</testSuiteGuid>
   <testCaseLink>
      <guid>3c582c92-7a4d-48bd-8496-614733a05ffb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Address_Map_Details/C2SC_Test_to_check_the_map_option_is_navigating</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b5d92c9-5abf-42a1-9813-11088058604f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Address_Map_Details/C2SC_Test_to_check_the_map_option_is_shown_in_the_My_Profile_details_page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
