<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10460_C2SC_SetPasswordEnhancement</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>7d487f3e-cd27-4d74-8bf9-9b8ee6c4554a</testSuiteGuid>
   <testCaseLink>
      <guid>9f49732c-a452-4030-90ee-1104aacf4df8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_SetPasswordEnhancement/C2SC_Verify consumer is able to view the entered password on click of eye icon from both the fields</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
