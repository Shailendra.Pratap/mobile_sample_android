<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_MyJourney</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>dace4880-8c38-46e8-8480-929529db32dc</testSuiteGuid>
   <testCaseLink>
      <guid>88591751-7a13-4162-9101-db8215d70bf5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_MyJourney/C2SC_Verify_the_back_button_in_My_Journe_page_in_Homepage_top_right_corner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eabd9709-1851-4094-a3c9-6b0839fae28c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_MyJourney/C2SC_Verify_the_icons_in_My_Journey_page_in_Homepage_top_right_corner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b3146db-b89e-4b3b-833f-230a5fe1bc8d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_MyJourney/C2SC_Verify_the_My_Journey_button_content_in_Homepage_top_right_corner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67dd5bdb-b5b0-41a2-93d8-3fbde40be47c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_MyJourney/MyJouney</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
