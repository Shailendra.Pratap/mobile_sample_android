<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_TodoScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>3fad8ba4-90d9-463a-aaee-d782cbfd36d5</testSuiteGuid>
   <testCaseLink>
      <guid>d9d1b0ae-9152-4e6f-b71e-a66182a98e99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_SellerFlow_Verify_that_Schedule_Home_Showing_task_is_present_in_To_do_tab_in_home_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8b7ee8b-0d69-4b33-9390-191e166a2ae2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_SellerFlow_Verify_the_input_fields_present_for_schedule_home_showing_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d2eb1ec-8dd4-4d4b-9a99-ed8f7ef0417b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_To_validate_the_View_task_displayed_on_clicking_the_self_assigned_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cb6e58f5-d1ea-4f12-b29c-129492b165e0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_To_verify_clicking_delete_option_for_a_self_assigned_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c7dd5c7-9c23-4a33-9691-35a9b97ec29c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_To_verify_clicking_edit_option_for_a_self_assigned_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7a9928f8-0da4-46c4-8c82-6c2ef01e2dfd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_To_verify_consumer_can_enter_data_in_company_field_on_Schedule_Inspection_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>779af84d-34df-4310-9b86-25bfc68493e4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_To_verify_marking_a_self_assigned_task_as_done</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>439a5834-6ec6-452d-8acf-ef4bba7717c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify Consumer is able to see the Congratulations on listing your home with Coldwell Banker todo task on successful registration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e6a8406-8ccb-493f-b839-07ce8a79f8e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify Consumer is able to view the content of to-do task and perform events</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3a67e884-98e2-4e58-b23f-4402e47da2e6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify that schedule closing to do task, then the date will be blank if consumer has not completed task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2227e4b5-7026-4075-8099-7fe5f9fdf3a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify the closing details displayed in the homepage on closing day when schedule closing is not done and title company_is_not_chosen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>382dd3dc-af12-4378-8775-1e41c0bb2f21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify the closing details that is displayed in homepage on closing day when schedule closing is done and title company is chosen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>808affe5-1da3-4368-92f5-fa638d6ee62b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify the Closing Reschedule task in Timeline got updated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5d21f902-68d7-4729-b693-e519b77c0c1f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify the fields in reschedule closing screen when user selects Rescheduled</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>84c0457e-0b57-496f-82cf-f1eae503952a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify the options under What went wrong</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa5aee31-f194-42be-b577-4d3d30f5f620</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify the rescheduled closing date is updated at the top of home screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1315dc4-a62f-4408-a1d3-4ccf0df45812</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify to-do list is not viewed until the flow is completed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b6500ea-8f95-4dd5-adde-448d820a1b61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_all_fields_are_present_on_Add_Item_screen_as_per_design</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78d4416a-10e7-44e1-a6a7-df4e02ca44c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_buyer_able_to_add_the_task_when_enter_data_all_input_fields</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2eb993ff-d398-481a-a9c4-c98c1220ad46</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_Closing_Documents_Available_description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>391c7d34-e4a1-4e2c-8015-607d39cff98d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_discarding_the_self_assigned_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60b7ed30-e82f-4a03-b58b-71781b83ed04</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_display_of_error_message_when_required_fields_not_added</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>386f7ec5-0b86-448c-8dc2-d106147e4fb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_marking_a_self_assigned_task_as_done</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>52cb1eaf-640b-484f-b6c3-a426be7ec008</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_Obtain_HomeOwners_Insurance_title_and _description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0cfbfe53-fe1b-4705-b4d0-946b7b5e21b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_on_click_of_close_icon_when_no_data_is_entered_buyer_should_navigate_to_My_Home_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bcfb1962-f0b5-4dd5-b3d4-e7c68127d4d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_pop_up_message_is_displayed_when_buyer_filled_form_and_click_close or back_icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4b556a21-29df-4ac2-884e-3d5d8db557e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_saving_the_self_assigned_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>92cc1098-32f3-4e92-8198-f5d398dc0a36</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_Schedule_Closing(Calender) _title_and _description</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f9a487da-8515-4a9e-88af-3d1b76925ea3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>17f8c465-75bd-4836-bb15-13142a44f183</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_Schedule_Inspection(Calender) _title_and _description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93bbe4e8-ef11-40c6-8f66-708be882e799</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_Schedule_Inspection_Time_picker</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4db287f0-7aaf-45da-9ead-676534be40da</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_SellerFlow_error_messages_displayed_for_Date_Start_time_and _nd_time_fields</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3f4a26cb-605d-4719-926f-644ee39d4958</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_Submit_your_initial_earnest_money_(payment) _title_and _description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2234394f-2c81-405b-a788-0c506bffd0d5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_the_appearance_of_Schedule_Walk_Through_task_in_To-Do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf6ce5b5-5bc6-4cdf-ad13-529e51f6b785</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_the_fields_shown_for_system_task_Schedule_Walk_Through_in_To-Do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d47350ab-fc8d-424d-b212-a1d89cc50042</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_the_Schedule_Walk_though_form_which_contains_the_required_fields_and_respective_error_messages</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>579036ad-c834-4ce7-858d-994df153ca37</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_the_status_of_task_on_click_of_Add_in_the_Schedule_Walk_though_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c9cffad2-3d5f-40f5-b59a-9249a3016294</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_Title_commitment_form_received_(pdf)_title_and_description</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9045b74-d7aa-4cc8-8ee4-5756c23a1fb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_when_Add_Task_form_is_saved,_that_task_should_be_listed_under_To_Do_tab_on_Home_Screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6011f317-a7b8-4238-b47c-3bba3b073b9a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_when_buyer_clicks_Cancel_button,_buyer_should_stay_in_Add_Item_screen_and_edited_fields_remain_intact</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7348525-5a9f-43f2-8619-7d0c486ef088</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_when_buyer_clicks_Discard_button_buyer_should_be_navigated_to_Home_page_and_edited_task_is_discarded</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3efa3b24-72f2-4759-a4b6-4138e5bde9c2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C2SC_Verify_whether_the_user_is_able_to_view_the_header_and_number_of_to_do_tasks</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fbf679c4-82e2-48cd-9444-cf86cf906b26</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C541196_C2SC_Verify_the_fields_and_respective_error_messages_in_schedule_closing_form</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af22f764-2188-423b-bb81-49a0db67ab1b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>186afa38-95cd-495f-8f14-53dfb149840b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C541197_C2SC_Verify_the_status_of_task_on_click_of_schedule</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>36644583-3dff-402c-9d2c-8246a656332a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>aa6dab9b-90c7-44f6-a019-195305181702</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C569843_C2SC_To verify self added GRA contact name display in team dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d5d79226-0cbd-4d86-b108-28f408d193c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/C569844_C2SC_To_verify_consumer_can_edit_Team_member_on_editing_ToDo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36b32e8f-3e8f-48f0-a0f8-20809d20196b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/SellerFlow_Verify_that_Provide_Wire_Instructions_to_Title_Company_task_is_not_available_4days_prior_to_closing_date</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa3c4955-2417-4644-9a17-388e67f06539</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/SellerFlow_Verify_that_Provide_Wire_Instructions_to_Title_Company_task_is_not_available_for_buyer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41b89471-4a15-4a75-bf4d-69e06d597c5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/SellerFlow_Verify_the_contents_of_Provide_Wire_Instructions_to_Title_Company_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fea2cac9-b858-47be-b837-699f8273659d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/SellerFlow_Verify_the_functionality_after_clicking_on_Confirm_button_in_the_Provide_Wire_Instructions_to _Title_Company</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bd3ea3a5-42ad-4589-9622-4d075fe1fceb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/SellerFlow_Verify_the_Title_Due_and_Description_for_the_task_Provide_Wire_Instructions_to_Title_Company_in_ToDo_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>60839362-1022-4971-b07d-733cf4f0f70e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_My_Docs/To_verify_the_addition_of_counters_to_contract_section_in_My_Docs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>471b1dbb-850e-47e0-a229-97583460204d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TodoScreen/Verify_the_contents_of_Review_ListingAgreement_screen_displayed_SellerFlow</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
