<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_Homepage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>30a0d111-2110-4051-9f70-c9ac5d526d2d</testSuiteGuid>
   <testCaseLink>
      <guid>e3cd9c4c-3243-4bd0-a6d1-f7e00a70ff53</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Buyer_Verify_My_Teams_page and mortgage_details_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a3bd7db-a49d-4df2-9459-cea8399e04c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Buyer_Verify_the_Agent_details_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0d431ec4-642a-4246-8470-4936e0785a00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Buyer_Verify_the_closing_status_in_top_of_home_page_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8320a3dd-d3e8-4cae-bec5-c507c39b7299</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Buyer_Verify_the_My_Docs_page_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85de4dbb-0ba3-4f6f-9e31-2dc7845a01b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Buyer_Verify_the_toggle_option_to_switch_between_the_transactions_in_home_page_and_my_profile_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e4cea046-f886-486a-83de-1b3143878f8f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Buyer_Verify_the_To_Do List_and_Timeline_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a14e07e2-d3c7-4558-be94-4703e2177938</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Buyer_Seller_Verify_the_toggle_option_to_switch_between_the_transactions_in_homepage_and_my_profile_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0a925cea-f495-4dee-a025-30d3dec0ba87</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_To_verify_display_of_closing_date_and_number_of_days_to_closing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93d3b6ac-c66f-42fd-a3eb-709d99236506</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_To_verify_display_of_Closing_Day_in_place_of_estimated_difference</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>973f8f34-fcb5-428f-aa1c-aad07bf177d0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_To_verify_display_of_on_track_Icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46dc1851-7a0c-4ecf-9d44-eb08332bffcc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_To_verify_Homepage_validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b2a33660-11ec-43cd-8a1a-356222188e88</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_To_verify_task_category_contents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d75d8e9-e1ea-4783-8f6e-d959bf97b4f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify the heading on top of My Docs page got changed from C2C to HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>28d8c776-da17-49d7-9c35-22dc86d8e886</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_consumer_is_able_to_see_Transaction_Completed_screen_once_the_Transaction_is_closed_successfully</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af3b5b2f-9689-4490-b633-50950fec26a7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f6631b8a-dce9-4670-bdae-ed8c723d77f1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c6ff830d-59ab-4b18-a295-30010d1e22d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_consumer_is_able_to_use_all_the_features_as_it_is_after_closing_the_Transaction_successfully</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d70a1be3-cdae-482b-857e-999154ef0604</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5549cdd0-3638-4fda-bdc4-995227772741</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>725ab1b6-739f-4bbc-90d1-394b61988f6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_Mark as_done_under_submit_your_initial_earnest_money</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75bd3989-873f-4d9a-8a55-418baa52f2b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_Mark_as_done_in_the_to_do_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a1e6acd9-6df2-439f-9dcf-891deccd3ade</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_added_notes_are_displayed_for_second_time_when_we_open_task_if_consumer_opted_for_Save_Continue_Later</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff6e2624-32ae-4559-b697-9247cce8eff4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_added_set_of_notes_are_adding_as_a_list</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>30213203-de84-4c4a-8838-4e58845be855</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_Note_to_Buyer_is_appearing_in_to_do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f77f50e-c778-481a-a674-019c0fb90b7b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_Note_to_Buyer_task_is_moving_to_Timeline_when_we_mark_as_Done</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>097263d7-faf3-4286-b6a8-143cd94c383a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_no_document_is_available_for_the_to_do_task_when_a_no_service_provider_is_selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>64b19c4f-c081-40f2-bcde-9d24aad0d8eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_no_document_is_available_for_the_to_do_task_when_a_recommended_service_provider_is_selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>051f55cc-dc3c-4fca-b0b7-cc14dd4e4a28</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_no_document_is_available_for_the_to_do_task_when_a_self_preferred_service_provider_is_selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a5efe18-647d-4c6b-a727-acc7f171134c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_that_title_Schedule_Open_House_is_displayed_in_To_Do_page</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af3b5b2f-9689-4490-b633-50950fec26a7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f6631b8a-dce9-4670-bdae-ed8c723d77f1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>d1675a5f-cfb2-4ad4-b016-731e58e4586e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_appearance_of_obtain_homeowners_insurance_and_closing_documents_in_to_do_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6471b6e2-60ff-474f-ba83-8b6c0732d9b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_appearance_of_Submit_your_initial_earnest_money</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7247eeab-6b7f-4539-9d96-110854deff46</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_close_icon_X_in_schedule_inspection_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>426c6d31-aba2-40a4-bdec-09bf96155fc9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Date_field_in_the_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80ca1dd5-2200-487e-8057-fc5796f8d5c7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Description_text_Note_to_Buyer_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0eeb3da2-45f7-4016-b136-1a8a51e35819</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Due_days_for_Note_to_Buyer_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4fbeb996-0072-40eb-96dc-206dd80485b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_error_message_displayed_for_date_field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a62d29e-776f-4823-aa72-661250f759bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_error_message_displayed_for_End_Time_field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>341c4a5a-0913-4b4c-ae6a-e3f3fd816922</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_error_message_displayed_for_Start_Time_field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dbee32f7-c07d-4d78-bb3d-d4753a59699d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_error_message_for_Time_field in_schedule_inspection_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd74024e-1ea7-4978-98ac-b85fcc8b25b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_fields_shown_for_closing_documents_task_in_to_do</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af3b5b2f-9689-4490-b633-50950fec26a7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f6631b8a-dce9-4670-bdae-ed8c723d77f1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>fec6490d-d14b-43c7-a65c-586a0360bf47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_fields_shown_for_obtain_homeowners_insurance_task_in_to_do</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e31ff90b-302b-4c8d-b87e-b4cb476fcf97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_fields_shown_for_Title_commitment_form_task_in_to_do</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af3b5b2f-9689-4490-b633-50950fec26a7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f6631b8a-dce9-4670-bdae-ed8c723d77f1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>cc305e5a-4b4e-4d3a-ac20-675aa42f8096</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_Home_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d6fa030-ed4c-4526-b5ab-1433c6cf3109</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_My_Agent_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>94eb5c5d-d158-4054-a850-b6bb80691986</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_My_Profile_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>14918646-f634-4dbf-a215-767a15ab09be</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_heading_on_top_of_My_Team_page_got_changed_from_C2C_to_HomePlace</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d7fb508-588f-43d6-a46a-e7d50a34e239</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Inspection_Company_field_in_the_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bbd3b8ad-56bb-498b-bb4c-318407f72e96</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_label_of_the_dropdown_for_seller_login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>01a9a62d-b57f-4f32-a6b7-34fd59582915</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_mark_as_done_button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90aec9a5-b7d2-4285-b5e7-fd0e570f433b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_options_under_submit_your_initial_earnest_money</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5f41b951-cbba-4e69-a8fe-daeb6f80b04e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Save_and_Continue_Later_button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f92d6417-ab13-4403-866d-e45981acd4d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Schedule_Inspection_on_clicking_of_done</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05b385a6-4247-42ba-99bc-1fc4f2fca5a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_sequence_in_Todo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f5dec1b2-2a8d-4d01-98fe-4ae7286e6c40</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_verify_the_status_on_click_of_Obtain_Homeowners_insurance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33261939-4cf9-45ac-8516-392cd188001b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_SubText_for_text_Note_to_Buyer_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39a4883e-a73b-4f97-b7e7-50122bf56aec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Time_field_in_the_form</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18fbebce-a073-4442-b634-3da6842a80b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Title_description_due_days_for_Submit_your_initial_earnest_money_task_in_todo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d2bbfa2-46b0-4405-bc05-07517cff60fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_to-do_page_and_timeline_after_adding_Schedule_Open_House_task</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0adcc930-b98f-4307-8306-8a7f79881b38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/C2SC_Verify_the_Wire_Funds_for_closing_pdf_task_in_to_do</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>af3b5b2f-9689-4490-b633-50950fec26a7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f6631b8a-dce9-4670-bdae-ed8c723d77f1</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e1b6b570-78e3-4321-8b55-76d754cc6631</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Seller_Verify_My_Teams_page_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a8a0b3e8-5e6c-4f0f-9abf-d32b682e6329</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Seller_Verify_the_closing_status_on_top_of _home page_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33444fbc-93ea-4e5e-839f-0d3945e75007</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Seller_Verify_the_My_Docs_page_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a73bed5-3f75-42b3-bd75-dad43a159ec5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Seller_Verify_the_toggle_option_to_switch_between_the_transactions_in_home_page_and_my_profile_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bde3eeb9-b7ba-454a-b9f2-3cc38bfea509</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Homepage/Seller_Verify_the_To_Do_List_and_Timeline_updated_by_the_selected_buyer_seller_transaction in_dropdown</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
