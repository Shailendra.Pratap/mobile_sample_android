<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_LoginScreen</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f1c0b77e-9565-4f2d-8659-595e5d077427</testSuiteGuid>
   <testCaseLink>
      <guid>15b0540b-5cb7-4a94-ad88-12edc2a6d900</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_TermUsePrivacy_Verify_the_terms_and_conditions_page_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a5ba73f9-0151-48c4-8cef-b67ba7e4705b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Test_to_check_the_login_page_entities_from_Registration_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0219c2ee-87fa-4d15-8480-e0d5bcd29ac6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_To_verify_login_screen_display_upon_relaunching_app_when_consumer_completed_registration</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c7a67b8-2a21-4bbf-951e-646cbc4c110d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify the message when user tries to Reset the password with correct email ID</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4183386a-9dc6-4c90-a7e6-de2d954c3ca0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_the_alert_message_is_displayed_on_clicking_Dont_have_an_auth_code_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1606ece-220e-4b93-b9a7-dceedca8b1bc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_the_error_message_displayed_on_entering_an_invalid_code</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>44165bfb-413d-4dcc-b6e5-ed1397798b12</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_the_Privacy_policy_page_is_displayed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3848aa3d-a2bd-45d1-a907-44df5e731135</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_the_user_able_to_enter_the_mandatory_details_in_Authentication_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7857cec3-df9e-41f1-ac6a-c6d28d6b5cb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_the_user_is_redirected_back_to_Authentication_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bca22407-952f-402b-a0b7-1f9d7fe37e79</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_the_user_is_redirected_back_to_authentication_page_from_privacy_policy_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2befb9ef-3fdc-49c3-a7be-ef61fda8b090</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_user_able_to_view_profile_validation_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c4e4758-d156-42bc-99c2-3948d40d5a54</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_user_able_to_view_terms_and_conditions_from_Profile_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b9295ae2-e036-42ce-b156-2e6d6f48e6c4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_LoginScreen/C2SC_Verify_user_able_to_view_the_Authentication_page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
