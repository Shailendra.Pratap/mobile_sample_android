<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R12068_My Docs_RegressionTC</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>fc1bfee4-e60f-4bbf-83ba-387eaa924a3a</testSuiteGuid>
   <testCaseLink>
      <guid>17ad8a69-3949-4ea9-bafb-e7cba4f0c21c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_My_Docs/C2SC_Verify _the _document_ icon(pdf) in _an_ empty_ bucket(Category)</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7340cd0-1c62-49a2-960e-8453d1fe7105</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_My_Docs/C2SC_Verify _the_ appearance _of _Date_ under _particular_ document _only_ when there is a document in place</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d62cac0e-1a9b-42e7-ae70-83cc62aae406</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_My_Docs/C2SC_Verify_My_doc_Screen_changes_in_my_docs_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d1ec86b7-3039-4d37-9fc9-850d59661315</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_My_Docs/C2SC_Verify_the_ability_to_sort_the_documents_based_on_document_status_in_My_Docs_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>061a02de-fd2d-4f2f-8526-6c1338af7ede</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_My_Docs/C2SC_Verify_the_default_sort_of_the_documents_based_on_document_status_in_My_Docs_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a6ad72b8-52c2-4a25-9292-c8783efcecc3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_My_Docs/C2SC_Verify_the_sections_ under_My Docs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf2b823a-3224-4944-a1f9-d05841c99960</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_My_Docs/C2SC_Verify_the_title_and_expand or collapse_option_for_Contract_and_others_section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>19363577-6ea0-4c54-84a9-bf8d6cace7a1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_My_Docs/C601916_C2SC_SellerFlow_Verify_that_customer_is_abl_to_see_Listing_Documents_section_under_My_Docs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5cb7b201-61f0-403c-a795-fa4d43e95f4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_My_Docs/C601917_C2SC_SellerFlow_Verify_that_customer_is_able_to_see_contract_Documents_section_under_My_Docs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fc225ddb-ecda-4826-8181-c2f5eca99df5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_My_Docs/C601918_C2SC_SellerFlow_Verify_that_customer_is_able_to_see_Title_Documents_section_under_My_Docs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>738f23d7-74c6-41be-96f2-b859910a2a4b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_My_Docs/C601919_C2SC_SellerFlow_Verify_that_customer_is_able_to_see_Inspection_section_under_My_Docs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>027e3c19-ea7f-4fb1-867f-3dfbbeee8a44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/C2SC_My_Docs/C601920_C2SC_SellerFlow_Verify_that_customer_is_able_to_see_Others_section_under_My_Docs</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
