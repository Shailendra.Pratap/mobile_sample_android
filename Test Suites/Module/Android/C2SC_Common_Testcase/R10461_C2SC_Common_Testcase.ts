<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_Common_Testcase</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>d15793b6-6a4d-4980-a45a-6a07b19e29dc</testSuiteGuid>
   <testCaseLink>
      <guid>233e76c5-d9f6-4f7b-921c-816b0e7f7f61</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Common_Testcase/C2SC_Consumer_Profile_Setup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d30d9937-a2aa-4b25-b013-d8eec0158555</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Common_Testcase/C2SC_EmailLogin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c1f5e60-8ed1-4dc0-aeb0-245a6baab3a0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Common_Testcase/C2SC_EmailLogin_Seller</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fa30959b-244a-4e31-b1bb-404d0e19292e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Common_Testcase/C2SC_HomePageFlow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>212e6327-bf25-4181-9d6d-e5b7d205031e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Common_Testcase/C2SC_HomePageFlow_Seller</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3d405889-2df9-4ebf-aa14-4feefe2502b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Common_Testcase/C2SC_Login</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>b0bd6794-0bd1-4c65-ac3e-abc1faa06811</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>59420827-951f-44e4-af88-4b77df807ca3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>505d99f9-d5c8-49fb-99c3-3636cb9d40d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Common_Testcase/C2SC_Login_Seller</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>177a367e-9672-42db-9d25-990de6511ae3</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>80f40bbf-f9ce-4602-b6b9-acf522fd9f9d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>1dd95192-a315-4732-9794-9e3d511328ac</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Common_Testcase/ios_samplelogin</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bf39274b-2551-469b-8fd7-c09a431a603d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Common_Testcase/Newpassword</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4d5434a-6a3e-4be3-88c4-5a918e99080a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_Common_Testcase/sampleios</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
