<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>C2SC_ProfileSetUp</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>6fa5bed5-bddd-4469-90d8-7ea863d984ba</testSuiteGuid>
   <testCaseLink>
      <guid>eb83d3c3-8667-4f27-81c0-dc02236ef56f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Consumer_Profile_Setup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d7765ae2-3d73-4658-bd30-085c88bfa8b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Test_to_check_the_details_shown_in_the_my_profile_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77736ef9-5ff0-40d3-bac0-e14935f63969</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Test_to_check_the_navigation_from_my_profile_page_to_home</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a55ae8b6-b455-44c2-b854-1ca24c9c2bc6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verification_of_tell_us_about_page_when_no_information_is_updated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8082078-e61e-4339-af29-6fc6e96beaf0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify if Remember Me option is clickable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73a0e589-ac38-43de-ae40-9473d5dd7911</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_About_app_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c91cd18-c010-4f5c-adc7-356c7c1b2cce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_buyer_is_redirected_to_tell_us_about_you_screen_on_click_of_ok_button</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8a9e8ed-93f2-4bc2-a88f-a993eaa87fe6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_change_password_in_my_profile_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c6d322a0-5a55-48dd-ac57-385e3f6d0283</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_condition_to_update_the_password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>025f9969-6de9-4ffb-95de-4a1928c46f57</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_display_of_error_message_when_confirm_password_do_not_match_with_new_password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3135bdeb-794b-4931-8f24-0fe285bab22d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_fields_under_your_information_section_are_editable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c06e3fd1-b071-44f9-a434-f2c857ab7e80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_fields_under_you_are_buying_section_are_not_editable</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ba234ea-2fb2-43f7-89f4-b5eae09c4f29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_logout_option_for_seller_flow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2abfb576-1ad1-4588-90c0-8f1e20aa6e40</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_navigation_to_NewPassword_Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>49c3030c-ad0c-44ba-a10b-2073a32fc0f1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_Privacy_policy_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9cccde86-9129-4b0d-bab9-532d4689ef4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_Rate_us_option_in_seller_flow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d28bdc4-ca40-4132-8186-21c397009376</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_Terms_of_Use_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39f6882a-14e6-4017-bcfd-147a9f0fb7e9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_that_password_updated_succesfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e2151b2-a80a-4a66-b739-37f5d8985131</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_the_7_settings_segment_options_are_retained_in_My_Profile_for_seller_flow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8d6fce5-904e-4a6d-b67c-6269758cbbb6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_the_contents_of_registration_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3fdea4de-3a3b-482b-9041-a953549dd258</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_the_error_message_for_all_mandatory_fields</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>76f02e3f-1b05-4675-841f-8c7dda3dcc42</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_the_label_under_the_profile_image_in_My_Profile_page_for_seller_flow</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f788b7c-a665-4b0c-af20-b7d71cbac0dc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_the_logout_functionality_of_C2C_app</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a9472c0-878d-4c21-949d-bcca93346b80</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_the_password_is_encrypted</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b834f835-1034-49e7-a6d9-fd09f309c18a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_the_presence_of_Remember_Me_checkbox_in_login page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6eed646e-d8a1-4259-9cdd-6cdecdcdedde</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_the_Property_details_are_prepopulated_under_You_are_selling_section</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9de9435f-0e03-413f-95c9-664f7678dba8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_the_sections_in_Profile_setup_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8be65041-8f75-409b-a4ee-848e7ddfbc2d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_the_Seller_type_which_is_chosen_in_Tell_us_a_little_about_you_page_is_displayed_under_Email_in_My_Profile_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20c33a2b-9ca1-4cdf-89ce-eb8a42ff19cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ProfileSetup/C2SC_Verify_the_settings_options_under_profile_details_in _My Profile_screen</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
