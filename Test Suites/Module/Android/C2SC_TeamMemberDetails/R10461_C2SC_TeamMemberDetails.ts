<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_TeamMemberDetails</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>451936e5-a2ea-4470-a4f0-d5d6cd12fd74</testSuiteGuid>
   <testCaseLink>
      <guid>4d4e7a66-62d1-48b3-9bde-a74b1c7a993b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify _that _customer_ should_ be _able _to_ delete_ the_ recommended_ service_ provider_ information</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7aaea1a1-3b4c-4e1f-85b5-171d41b192b7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify _that _on _clicking _x_ the _buyer_ is _redirected _to _the _My Teams_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a3b14c7-9a86-4756-ade0-a721ee93eede</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify _that_ customer _should _be _able_ to_ delete _the _Preferred _Service_ Provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b2479c9a-09d5-4740-aa60-799a26264a6e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify _that_ customer _should_ be_ able_ to_ delete_ the _newly_ added_ custom_ team_ members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c0d1255-90b8-49ee-bdc7-d4b293fe36ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify _the_ discard_ option _when_ no_ changes_ are _made_ for_ newly _added_ custom_ team_ members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41d0827d-2d3f-4d90-a538-f81412982642</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify _the_ Recommended _Service_ Provider_ under _My _Teams</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5769179e-c943-43b7-92cf-99af22213eba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_ that _customer _can_ be _able_ to _edit_ the_ preferred _vendor_ details_ for _preferred_service _provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>160b6158-8987-49b1-a726-d3deb7a90e4f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_ that _customer _can_ be _able_ to _edit_ the_ preferred _vendor_ details_ for _recommended _service _provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>96d6113d-7eb6-4fd1-804e-c8bcb8adcead</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_ the _cancel _option or close(x) _option_ for_ recommended _service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>877af1b0-18d3-4149-bc2a-6ce1d725220a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_ the _contents_for _custom _team _members_self_added</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e71a29e-b6fa-4ca2-8436-6d86eaf352c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_ the _discard _option_ when _no changes _are_ made _for_ recommended _service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e127476-0f98-40a0-9a37-4c524f865323</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_ the _Save _in _the_ edit_ popup_ for_ preferred_service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>222f8caa-58d4-4a98-88fe-27bd0bfcc88b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_ the _Save _in _the_ edit_ popup_ for_ recommended _service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7cd17790-34d6-4470-ae60-408513c91828</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_ the_ discard and cancel_ in_ close_ popup _for_ recommended_ service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b811c58b-6524-4ebd-beff-d44634b0e561</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_buyer_is_navigated_to_Add_Team_Member_screen_on_clicking_Plus_icon_on_My_Teams_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ec26b2c-d4a3-4e66-aa09-63dc297513ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_Create_Team_option_in_My_Profile_page_for_adding_team_member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>07bdce8d-b158-45dd-ab03-b72bf6a28c21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_error_message_is_thrown_when_input_fields_are_blank_and_click_plus_icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5bea0cc2-ef90-41a3-bbe7-b1f243670589</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_error_message_on_not_entering_phone_number</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fb664adf-92a4-4933-93aa-f0c27c88c9d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_error_message_on_not_entering_the_Email_id</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dcb78081-df23-4287-8291-f0bd3cf9d944</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_on_click_of_Close_icon,_buyer_is_redirected_to_My_Team_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7e092239-c7f4-4366-8f16-e8b11aa93c06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_that_if_the_field_is_empty_then_there_will_be_no_response_on_clicking</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae3cabed-e61d-457d-af49-19a77befa206</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_that_the_user_able_to_Add_a_new_Team_member_for_inspection</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>470811b5-797c-4b67-87de-ec77289d9c67</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_that_user_able_to_navigate_to_Add_Team_member_page_from_My_profile_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>da75e74d-5941-46ec-8b4b-6ddb77caa48a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the _discard_ and _cancel_ in _close _popup _for _newly _added_ custom_ team_ members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b761b238-4dc8-4c42-b4f9-884e39739101</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_ cancel_option or close(x) _option _for_newly_ added _custom _team _members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f55e0f7c-d7e4-4653-a9c7-85614f934827</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_Accept_option_for_default_service_provider_Title_Closing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ee11ad16-f038-413c-bbd4-31debba80b86</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_Cancel in the delete popup for recommended service provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c98911a4-e63f-4a8c-b30f-2032c9720be8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_Cancel _in _the _delete_ popup_ for_preferred_ service_ provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>98b0d835-78d2-4949-979b-95a1953844fd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_Cancel_in_the_delete_popup_for_the_newly _added_custom_team_members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4826d21b-bfa0-43f8-9443-1747f4b7602b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_error_message_for_Contact_name_field_in_Add_team_member_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ed2264d2-a1fc-4890-9c78-e0b90a8b4a7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_error_message_for_Contact_name_field_in_Edit_team_member_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4c719dc5-ca0e-4a1e-a978-a3c118769b79</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_error_message_for_email_Id_field_in_Add_team_member_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>268cc2e1-ed82-48c0-b567-376f4d24e3a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_error_message_for_email_Id_field_in_Edit_team_member_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4d3cafaa-8930-42f4-96ed-9fb66320f9b4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_error_message_for_Phone_field_in_Add_team_member_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7397715-1fc5-4153-b7c7-609b84bcbed6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_error_message_for_Phone_field_in_Edit_team_member_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e23267d0-9400-4996-b0b6-d38b48add31d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_error_message_is_displayed_for_contact_name_field</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2edb351a-d63b-47cf-9e50-c92ea497045e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_fields_in_Add_team_member_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>615f1d07-9b58-4794-813e-521aff1b8a75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_fields_in_Edit_team_member_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bad4ab45-35ea-4145-bed1-da3b7c05f36c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_Know_Why_content_in_My_teams_tab</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f56819db-39e4-4f81-a674-7a9bc000d3ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_Save_in_ the_ edit_ popup_for _newly_ added _custom _team_ members</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c83ad344-5594-44dd-8896-f0ba736f9d2c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_user_able_to_view_the_service_provider_beside_the_Mortgage_milestone</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eeed062c-350e-454e-98b0-0db69e1e3cdd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_weblink_for_recommended_service_provider_on_Team_members_contact_details_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a97a7e22-f54f-462e-b6c9-21195a48fca6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_the_Weblink_for_self_preferred_service_provider_on_Team_members_contact_details_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0dc75cc5-5b0d-46cd-9a65-4e9d9baee2fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_user_able _to_add_team_member_for_Attorney</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3443b426-c3a0-4b17-abbb-f3f48a27b54a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_user_able_to_add_team_members_for_others_category</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>093a4ce1-85a5-4d60-8bc6-c32ed6a11c92</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_User_able_to_select_the_service_provider_for_all_the_milestones</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cee1c5bc-d9b2-459f-a454-40a02476b66c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_user_able_to_view_service_provider_beside_Home_Owners_Insurance_milestone</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5640051e-d46e-4c13-a8d0-255df333a90d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_user_abl_view_service_provider_beside_the_Title_or_Closing_milestone</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9243bf33-b826-424d-9ba1-36dab9e0f950</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/C2SC_Verify_when_buyer_adds_team_member_name_and_category_display _on My_Teams_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a0699f3a-076a-4b5e-9e5f-2b9b2ee62a4c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/To verify the team member details page if Attorney is selected</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5504b2f9-2da1-4519-aebf-3304935a841a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/To verify the team member details page if GRA is not Realogy affiliated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ba03f6dd-ed88-4db1-b196-456ac52a372d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/To verify the team member details page if Inspection is selected from dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fcecf881-e201-4f34-8286-ffc1ff622741</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/To verify the team member details page if Other is selected in dropdown</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>50bacd4f-44b2-44e6-9da9-642d9a545def</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/To verify the team member details page if service provider is GRA</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b300df8e-6abc-4f8a-8a3e-721f50da1a37</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/To verify the team member details page if service provider is Home Owners insurance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2ae5fd2f-3a81-4855-9be5-c5f82f9d129d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_TeamMemberDetails/To verify the team member details page if service provider is Title or Closing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>693c9872-a447-457c-8cbb-6847dd32cf2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerFinancialsScreen/To_verify_consumer_navigate_to_Property_Insight_screen_on_click_of_Edit_button_beside_Insight_header</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cde49ebb-4b0b-4dcd-8037-e537d2cdcd21</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerFinancialsScreen/To_verify_content_of_Property_Insight_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>271c5369-857d-4938-9ee2-0b7fa755e7cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ConsumerFinancialsScreen/To_verify_entered_data_in_Property_Insight_screen_is_saved</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
