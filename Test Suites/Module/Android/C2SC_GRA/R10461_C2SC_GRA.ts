<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_GRA</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>ffdefc25-558c-4bf1-9995-4c0be3c8a7b8</testSuiteGuid>
   <testCaseLink>
      <guid>a5034220-00a6-45d5-95a4-69cc66e8a717</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C2SC_To_verify_contents_displayed_in_mortgage_provider_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6c9c0c34-c310-4761-82f4-3cb5e85382e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C2SC_validate_display_of_company_name_updated_via_edit_option _besides_Inspection_milestone_on_my_teams_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f1639847-e0c4-46f8-9f2d-35c5522076ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C2SC_validate_display_of_company_name_updated_via_edit_option _besides_Insurance_milestone_on_my_teams_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f2dbae9a-891d-4986-839d-d472465517b2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C2SC_validate_display_of_company_name_updated_via_edit_option _besides_mortgage_milestone_on_my_teams_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>86f9cb16-ad1b-4294-b8fb-259942d8010b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C2SC_validate_display_of_company_name_updated_via_edit_option_besides_Title_milestone_on_my_teams_page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5c0e2bd7-ed82-4f59-a90a-495b4799ab1f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C2SC_validate_the_error_message_displayed_when_blank_invalid_data_is_entered_in_input_fields</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0fa2df42-a21e-49ff-bd6b-f91bf93169cf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C2SC_Verify_consumer_is_able_to_see_asterisk_as_he_enters_the_SSN_value_after_selecting_the_preferred_mortgage_provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18c5e0ae-35b7-4fb5-b93d-ddf5dc1dd06d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C2SC_Verify_consumer_is_able_to_see_stars_when_he_finishes_entering_the_SSN_value_after_selecting_the_preferred_mortgage_provider</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e9e84c24-7e87-4748-999e-bff5b1b485d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C2SC_verify_navigation_to_edit_team_member_page_on_clicking_edit_option_from_default_milestone</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95639273-18e4-4879-8eac-246cd9ae26ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C2SC_verify_navigation_to_edit_team_member_page_on_clicking_edit_option_from_Inspection_milestone</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56cff3e9-9342-45f6-9006-e135a074c520</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C2SC_Verify_that_check_box_is_available_in_SSN_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aae047f9-f05c-4035-a964-07640e792d6a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C577704_C2SC_Verify_that_check_box_is_available_in_SSN_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ed26baf-0912-4795-9f8d-cbc704f8b739</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C577705_C2SC_Verify_the_description_for_the_check_box_in_SSN_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ab64301-1fb9-4b68-a808-a248a8d584ab</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C577706_C2SC_Verify_the_Save_button_is_disabled_before_selecting_check_box</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1060a4e6-3dda-4f8b-8c2a-4315f5cc03f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C577707_C2SC_Verify_the_Save_button_is_enabled_after_selecting_check_box</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e573b438-9748-4969-b0d6-baad9906b2d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C577708_C2SC_Verify_that_GRA_is_added_for_successfully_for_Mortgage</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd52dbd0-b4c1-4069-927b-de2e172f8173</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/C582851_C2SC_Verify_ if_recommended_service_provider_options are_displayed,_along_with_Accept_and_Edit_option</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c93ee36b-19e0-4471-a2d5-3e5fa7b59f79</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/Verify_the_status_Application_not_found_apply now_is removed from Mortgage-Non-GRA contact details screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a5b6c95-8d92-48c5-b171-be5639734511</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_GRA/Verify_the_status_Application_not_found_apply_now_ is_removed_from_Mortgage-GRA_contact details_screen</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
