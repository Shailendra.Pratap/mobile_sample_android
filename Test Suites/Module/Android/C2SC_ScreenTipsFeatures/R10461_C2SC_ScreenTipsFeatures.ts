<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_ScreenTipsFeatures</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>4940cfa4-99ee-4404-b6f9-ba9e258970cd</testSuiteGuid>
   <testCaseLink>
      <guid>1ce2da8a-654f-4abf-a5e8-60e1f98968ae</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ScreenTipsFeatures/C2SC_Verify_consumer_is_able_ to_ view _the_description_when_ he _focuses_ on_ the_feature_Contract</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f7e361c0-3b29-43d9-ad97-a3877a23da08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ScreenTipsFeatures/C2SC_Verify_consumer_is_able_to_view_ the_basic_functionalities_of_the_home_photo_by_clicking_on the_guide_icon</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8fedeb4-6b75-465c-bb51-4cb153227c06</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ScreenTipsFeatures/C2SC_Verify_consumer_is_able_to_view_the_ basic_functionalities_ of_the_different_features in _the_app_by_clicking_the_guide_icon_on_the_top</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1589318-7d6d-4144-849c-9d8559092241</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ScreenTipsFeatures/C2SC_Verify_seller_is_able_to_view_the_screen_tips_of_the_date_field_in_the_Schedule_Closing_to-do_page_by_clicking_the_guide_icon_on_the_top</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>654f0fef-65cc-4032-9db4-f307e305af68</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ScreenTipsFeatures/C2SC_Verify_seller_is_able_to_view_the_screen_tips_of_the_Inspection_section_by_clicking_the_guide_icon_on_the_top</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>47ad0810-9e36-42b9-9526-1ff40a8a953b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ScreenTipsFeatures/C2SC_Verify_seller_is_able_to_view_the_screen_tips_of_the_Listing_Agreement_section_by_clicking_the_guide_icon_on_the_top</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>650cc9e8-0178-4db1-a79c-101a1746116c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ScreenTipsFeatures/C2SC_Verify_seller_is_able_to_view_the_screen_tips_of_the_My_Journey_button_by_clicking_the_guide_icon_on_the_top</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68f71860-5a4b-4c77-b854-c749b045ec44</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ScreenTipsFeatures/C2SC_Verify_seller_is_able_to_view_the_screen_tips_of_the_property_icon_by_clicking_the_guide_icon_on_the_top</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51a915c5-1ce4-4ddc-8839-77fbc8859b75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ScreenTipsFeatures/C2SC_Verify_seller_is_able_to_view_the_screen_tips_of_the_Schedule_Home_Showing_to-do_page_by_clicking_the_guide_icon_on_the_top</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
