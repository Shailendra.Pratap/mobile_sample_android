<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R_12068_C2SC_AppLayout</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e33f1b2e-0c98-4aa4-a9a6-0c53403c0b24</testSuiteGuid>
   <testCaseLink>
      <guid>63aac609-548c-4310-bd1c-74cd4460ad33</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_AppLayout/C2SC_Verify_the_bottom_navigation_entities</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6e9b2339-8c61-4525-9c9a-60c4cca0bd57</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_AppLayout/C2SC_Verify_the_home_screen_entities</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>456ea350-0073-4915-8641-f555fe81e12b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_AppLayout/C2SC_Verify_the_top_navigation_entities</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
