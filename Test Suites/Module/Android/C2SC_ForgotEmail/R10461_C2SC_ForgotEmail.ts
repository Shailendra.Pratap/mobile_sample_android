<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>R10461_C2SC_ForgotEmail</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>9ece2d2e-9766-4ef6-bf5b-29e2dbbf27b6</testSuiteGuid>
   <testCaseLink>
      <guid>80b70cb8-09b1-4367-85a2-24c9d7879a85</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ForgotEmail/C2SC_Verify_the_error_message_for_invalid_code</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>432943a1-8fb8-4f8b-b20b-9c147a54404c</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>7415fcda-5a20-4f7b-9dde-0085772bb471</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3c029945-eb62-4888-affb-09c4abfd9f30</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ForgotEmail/C2SC_Verify_the_error_message_if_the_invalid_mobile_number_entered</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3fab5147-3b7b-48b0-916e-39a2a7cbc9b9</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d25dca0f-2be1-494c-a2a9-61441ff0fbf3</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>083677c4-f2da-4a03-a133-5a2d3e85836e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ForgotEmail/C2SC_Verify_the_Forgot_email_screen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a0e84d62-943e-4cfb-ad6a-0461b572ee91</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ForgotEmail/C2SC_Verify_the_retrieve_email</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1a38c25-eab5-4b74-9f69-80d7e526cc24</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/C2SC_ForgotEmail/Verify_the_forgot_password_screen_on_entering_registered_email_id</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
