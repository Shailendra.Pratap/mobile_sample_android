import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication(GlobalVariable.sAppID, true)

WebUI.delay(10)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('iOS')) {
	try {
	Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
}catch(Exception e)
	{
		
	}
}
Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_LoginScreen/input_Landing_text_Mobile'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_LoginScreen/input_Landing_text_Mobile'), 5, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_LoginScreen/input_Landing_text_Mobile'), '1122221234', GlobalVariable.intWaitTime)
if (AndroidDevice.equals('iOS')) {
	try {
	Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)

	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}catch(Exception e)
	{
		
	}
}

Mobile.tap(findTestObject('Object Repository/C2SC_LoginScreen/btn_SendSubmit'), GlobalVariable.intWaitTime)
Mobile.delay(10)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_LoginScreen/txt_InvalidUserMobileErrorMsg'), GlobalVariable.intWaitTime)

Mobile.closeApplication()