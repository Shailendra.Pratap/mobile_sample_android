import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_Login'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

not_run: Mobile.verifyElementText(findTestObject('C2SC_ConsumerDetailsScreen/txt_ValidateInfo'), 'Let’s validate your information so we can get started')

//Mobile.verifyElementExist(findTestObject('C2SC_ConsumerDetailsScreen/input_firstnamefield'),  GlobalVariable.intWaitTime)
Mobile.verifyElementAttributeValue(findTestObject('C2SC_ConsumerDetailsScreen/input_firstnamefield'), 'enabled', 'true', 
    GlobalVariable.intWaitTime)

//Mobile.verifyElementExist(findTestObject('C2SC_ConsumerDetailsScreen/input_lastNameField'), GlobalVariable.intWaitTime)
Mobile.verifyElementAttributeValue(findTestObject('C2SC_ConsumerDetailsScreen/input_lastNameField'), 'enabled', 'true', 
    GlobalVariable.intWaitTime)

//Mobile.verifyElementExist(findTestObject('C2SC_ConsumerDetailsScreen/input_emailField'), GlobalVariable.intWaitTime)
Mobile.verifyElementAttributeValue(findTestObject('C2SC_ConsumerDetailsScreen/input_emailField'), 'enabled', 'true', GlobalVariable.intWaitTime)

Mobile.scrollToText('Set up Profile')

//Mobile.verifyElementExist(findTestObject('C2SC_ConsumerDetailsScreen/input_phoneField'),  GlobalVariable.intWaitTime)
Mobile.verifyElementAttributeValue(findTestObject('C2SC_ConsumerDetailsScreen/input_phoneField'), 'enabled', 'true', GlobalVariable.intWaitTime)

Mobile.verifyElementAttributeValue(findTestObject('C2SC_ConsumerDetailsScreen/input_streetField'), 'enabled', 'false', GlobalVariable.intWaitTime)

Mobile.verifyElementAttributeValue(findTestObject('C2SC_ConsumerDetailsScreen/input_cityField'), 'enabled', 'false', GlobalVariable.intWaitTime)

Mobile.verifyElementAttributeValue(findTestObject('C2SC_ConsumerDetailsScreen/input_stateField'), 'enabled', 'false', GlobalVariable.intWaitTime)

Mobile.verifyElementAttributeValue(findTestObject('C2SC_ConsumerDetailsScreen/input_zipField'), 'enabled', 'false', GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)

//Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/btn_dialogOK'), GlobalVariable.intWaitTime)
Mobile.verifyElementText(findTestObject('C2SC_TellUsLittleAboutYouScreen/txt_TellUsALittleAboutYou'), 'Tell us a little about you')

Mobile.closeApplication()

