import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_HomePageFlow'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_MyProfilePage/tab_MyDocs'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_MyDocs'), GlobalVariable.intWaitTime)

Mobile.delay(5)

Mobile.waitForElementPresent(findTestObject('Object Repository/C2CS_MyDoc/txt_ContractDocuments'), GlobalVariable.intWaitTime)

String AndroidDevice = GlobalVariable.sDeviceAndroid
if (AndroidDevice.equals("iOS")) {
	Mobile.swipe(170, 640, 170, 490)
}

if (AndroidDevice.equals("Android")) {
		Mobile.scrollToText('Others')
	}

Mobile.verifyElementExist(findTestObject('Object Repository/C2CS_MyDoc/txt_Others'), GlobalVariable.intWaitTime)

try {
Mobile.verifyElementExist(findTestObject('Object Repository/C2CS_MyDoc/txt_UploadDocument'), GlobalVariable.intWaitTime)


}catch(Exception e)
{
	Mobile.tap(findTestObject('Object Repository/C2CS_MyDoc/txt_Others'), GlobalVariable.intWaitTime)
}
Mobile.delay(5)

if (AndroidDevice.equals("Android")) {
	Mobile.scrollToText('Upload')
}


Mobile.verifyElementExist(findTestObject('Object Repository/C2CS_MyDoc/txt_UploadDocument'), GlobalVariable.intWaitTime)
Mobile.closeApplication()
