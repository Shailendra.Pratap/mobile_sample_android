import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

lstReturned = WebUI.callTestCase(findTestCase('C2SC_API_Save_Invitation/C2SC_Get_Multiple_Transaction_Buyer_Registered_User'), 
    [('requestBodyPOST') : requestBodyPOST], FailureHandling.STOP_ON_FAILURE)

String sCollectionName = 'consumer-registration'

String sQueryColumnName = 'consumerUserId'

String sQueryColumnValue = lstReturned['emailId']

String sPhoneNumber = lstReturned['phonenumber']

String sTransactionN = 'transaction'

String stransColumnName = 'transactionId'

String stransColumnValue = lstReturned['phonenumber']

String sValueupdateStatus = CustomKeywords.'com.database.DataBaseConnection.UpdateDetailsDb'(sCollectionName, sQueryColumnName, 
    sQueryColumnValue)

String sValueupdatetransStatus = CustomKeywords.'com.database.DataBaseConnection.UpdateDetailsDbStatus'(sTransactionN, stransColumnName, 
    stransColumnValue)

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin_Registered_Buyer'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_MultiTransaction/btn_MultiPropertyScreenBtnExistingHome'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MultiTransaction/btn_MultiPropertyScreenBtnExistingHome'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/tab_FirstTransMultiple'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/tab_FirstTransMultiple'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Next', FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_TellUsLittleAboutYouScreen/btn_Next'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_TellUsLittleAboutYouScreen/btn_Next'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_AddToDoItemScreen/tab_ToDo'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_AddToDoItemScreen/tab_Timeline'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/tab_Timeline'), GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

Mobile.closeApplication()

