import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('C2SC_ConsumerFinancialsScreen/To_verify_consumer_navigate_to_Property_Insight_screen_on_click_of_Edit_button_beside_Insight_header'), 
    [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_SelectedServiceProviderProperyPageEditable/input_EstMonthly'), 
    GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_SelectedServiceProviderProperyPageEditable/input_EstMonthly'), '12', 
    GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_SelectedServiceProviderProperyPageEditable/input_DownPayment'), 
    GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_SelectedServiceProviderProperyPageEditable/input_DownPayment'), '10', 
    GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_SelectedServiceProviderProperyPageEditable/input_EstClosingCosts'), 
    GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_SelectedServiceProviderProperyPageEditable/input_EstClosingCosts'), 
    '15', GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_SelectedServiceProviderProperyPageEditable/input_MortgageRate'), 
    GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_SelectedServiceProviderProperyPageEditable/input_MortgageRate'), '124', 
    GlobalVariable.intWaitTime)
String AndroidDevice = GlobalVariable.sDeviceAndroid
if (AndroidDevice.equals('iOS')) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_SelectedServiceProviderProperyPageEditable/input_MortgageTermNoofyears'), 
    GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_SelectedServiceProviderProperyPageEditable/input_MortgageTermNoofyears'), 
    '2', GlobalVariable.intWaitTime)
if (AndroidDevice.equals('iOS')) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}
Mobile.tap(findTestObject('Object Repository/C2SC_SelectedServiceProviderProperyPageEditable/btn_Save'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_SelectedServiceProviderProperyPageEditable/txt_Estmonthlyvalue'), 
    GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_SelectedServiceProviderProperyPageEditable/txt_downpaymentvalue'), 
    GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_SelectedServiceProviderProperyPageEditable/txt_EstClosingValue'), 
    GlobalVariable.intWaitTime)

