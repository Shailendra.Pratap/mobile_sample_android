import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.apache.commons.lang.RandomStringUtils as RandomStringUtils

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_HomePageFlow'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

Mobile.tap(findTestObject('C2SC_TellUsLittleAboutYouScreen/tab_Home'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/tab_ToDo'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/tab_ToDo'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/btn_plusIcon'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/btn_plusIcon'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/txt_AddItemToYourList'), GlobalVariable.intWaitTime)

GlobalVariable.toDoTask = RandomStringUtils.randomAlphabetic(10)

Mobile.setText(findTestObject('C2SC_AddToDoItemScreen/input_Title'), GlobalVariable.toDoTask, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_AddToDoItemScreen/input_Description'), 'This is a test description', GlobalVariable.intWaitTime)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('Android')) {
    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/input_DueDate'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/icon_CalendarArrow'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/txt_CalendarDate'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/btn_Calendar_OK'), GlobalVariable.intWaitTime)
} else {
    Mobile.tap(findTestObject('Object Repository/C2SC_AddTeamMemberScreen/btn_Done'), GlobalVariable.intWaitTime)

    Mobile.tapAndHold(findTestObject('C2SC_AddToDoItemScreen/Img_CalendarIcon'), GlobalVariable.intWaitTime, 0)

    Mobile.swipe(170, 640, 170, 525)

    Mobile.swipe(170, 640, 170, 525)

    Mobile.swipe(170, 640, 170, 490)

    Mobile.swipe(170, 640, 170, 490)

    Mobile.tapAndHold(findTestObject('C2SC_AddToDoItemScreen/txt_CalendarDate'), GlobalVariable.intWaitTime, 0)
}

Mobile.swipe(170, 640, 170, 525)

//Mobile.tap(findTestObject('Object Repository/C2SC_AddToDoItemScreen/btn_AddToDo'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/btn_Save'), GlobalVariable.intWaitTime)


Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/btn_plusIcon'), GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

