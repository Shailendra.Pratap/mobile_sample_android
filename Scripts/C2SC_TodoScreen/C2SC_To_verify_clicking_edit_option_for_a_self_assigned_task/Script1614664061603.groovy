import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import org.apache.commons.lang.RandomStringUtils

Mobile.callTestCase(findTestCase('C2SC_TodoScreen/C2SC_Verify_buyer_able_to_add_the_task_when_enter_data_all_input_fields'),[:], FailureHandling.STOP_ON_FAILURE)

Mobile.scrollToText(GlobalVariable.toDoTask)

Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/txt_ToDoTitle'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/txt_ToDoTitle'),GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/input_TitleUpd'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_TodoScreen/btn_Edit'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_TodoScreen/btn_Edit'), GlobalVariable.intWaitTime)

GlobalVariable.toDoTask = RandomStringUtils.randomAlphabetic(10)

Mobile.setText(findTestObject('C2SC_AddToDoItemScreen/input_Title'), GlobalVariable.toDoTask, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_AddToDoItemScreen/input_Description'), 'Updated test description', GlobalVariable.intWaitTime)

if (Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_AddTeamMemberScreen/btn_Done'), GlobalVariable.intWaitTime, 
    FailureHandling.OPTIONAL)) {
    Mobile.tap(findTestObject('Object Repository/C2SC_AddTeamMemberScreen/btn_Done'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/Img_CalendarIcon'), GlobalVariable.intWaitTime)

    Mobile.swipe(170, 640, 170, 525)

    Mobile.swipe(170, 640, 170, 525)

    Mobile.swipe(170, 640, 170, 490)

    Mobile.swipe(170, 640, 170, 490)

    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/txt_CalendarDate'), GlobalVariable.intWaitTime)
	
	Mobile.swipe(170, 640, 170, 490)

  //  Mobile.tap(findTestObject('C2SC_TodoScreen/btn_EditCancel'), GlobalVariable.intWaitTime)
} else {
    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/input_DueDate'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/icon_CalendarArrow'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/txt_CalendarDate'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/btn_Calendar_OK'), GlobalVariable.intWaitTime)

    //Mobile.tap(findTestObject('C2SC_TodoScreen/btn_EditCancel'), GlobalVariable.intWaitTime)
}

//Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_TodoScreen/btn_Discard'), GlobalVariable.intWaitTime)

//Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_AddToDoItemScreen/btn_Cancel'), GlobalVariable.intWaitTime)

//Mobile.tap(findTestObject('Object Repository/C2SC_AddToDoItemScreen/btn_Cancel'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_TodoScreen/btn_EditSaveToDo'), GlobalVariable.intWaitTime)

Mobile.delay(5)

Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/tab_ToDo'), GlobalVariable.intWaitTime)

Mobile.scrollToText(GlobalVariable.toDoTask)

Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/txt_ToDoTitle'), GlobalVariable.intWaitTime)

Mobile.closeApplication()
