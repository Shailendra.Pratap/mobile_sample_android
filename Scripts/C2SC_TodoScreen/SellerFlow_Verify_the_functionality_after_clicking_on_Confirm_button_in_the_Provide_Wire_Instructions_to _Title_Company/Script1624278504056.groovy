import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

/**
 * @author MohanPri
 */
//lstReturned=WebUI.callTestCase(findTestCase('Test Cases/C2SC_API_Save_Invitation/C2SC_Get_Invitation_3daysPriorClosingDays'), [:], FailureHandling.STOP_ON_FAILURE)
//
//String sCollectionName = 'consumer-registration'
//
//String sQueryColumnName = 'consumerUserId'
//
//String sQueryColumnValue = lstReturned['emailId']
//
//String sPhoneNumber = lstReturned['phonenumber']
//
//String sGetColumnValue = 'invitationCode'
//
//String sValueupdateStatus = CustomKeywords.'com.database.DataBaseConnection.UpdateDetailsDb'(sCollectionName, sQueryColumnName,
//	sQueryColumnValue)
//
//String sValueInvitationCode = CustomKeywords.'com.database.DataBaseConnection.getDetailsFromDb'(sCollectionName, sQueryColumnName,
//	sQueryColumnValue, sGetColumnValue)
//
//println(sValueInvitationCode)
//
//String s1 = sValueInvitationCode
//
//char[] ch = s1.toCharArray()
//
//WebUI.delay(10)
//
//Mobile.startApplication(GlobalVariable.sAppID, true)
//
//WebUI.delay(10)
//
//String AndroidDevice = GlobalVariable.sDeviceAndroid
//
//if (AndroidDevice.equals('iOS')) {
//	Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime,FailureHandling.STOP_ON_FAILURE)
//
//	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime,FailureHandling.STOP_ON_FAILURE)
//}
//
//Mobile.setText(findTestObject('C2SC_CommonObject/input_Mobile'), sPhoneNumber, GlobalVariable.intWaitTime)
//
//Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)
//
//if (AndroidDevice.equals('iOS')) {
//	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
//}
//
//Mobile.scrollToText('Terms of Use', FailureHandling.STOP_ON_FAILURE)
//
//Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_1'), String.valueOf(ch[0]), GlobalVariable.intWaitTime)
//
//Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_2'), String.valueOf(ch[1]), GlobalVariable.intWaitTime)
//
//Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_3'), String.valueOf(ch[2]), GlobalVariable.intWaitTime)
//
//Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_4'), String.valueOf(ch[3]), GlobalVariable.intWaitTime)
//
//Mobile.tap(findTestObject('C2SC_CommonObject/btn_Submit'), GlobalVariable.intWaitTime)
//
//Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)
//
//try {
//	String newpassword = GlobalVariable.sPassword
//
//	println(newpassword)
//
//	if (!(newpassword.equals(null))) {
//		WebUI.delay(10)
//
//		Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_NewPassword'), GlobalVariable.intWaitTime)
//
//		Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_NewPassword'), GlobalVariable.intWaitTime, 0)
//
//		Mobile.setText(findTestObject('C2SC_CommonObject/input_NewPassword'), 'Mindtree@1234', GlobalVariable.intWaitTime)
//
//		WebUI.delay(5)
//
//		if (AndroidDevice.equals('iOS')) {
//			Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
//		}
//		
//		Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), GlobalVariable.intWaitTime)
//
//		Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), GlobalVariable.intWaitTime, 0)
//
//		Mobile.setText(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), 'Mindtree@1234', GlobalVariable.intWaitTime)
//
//		WebUI.delay(5)
//
//		if (AndroidDevice.equals('iOS')) {
//			Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
//		}
//		
//		WebUI.delay(10)
//
//		Mobile.tapAndHold(findTestObject('C2SC_CommonObject/btn_Continue'), GlobalVariable.intWaitTime, 0)
//	}
//}
//catch (Exception e) {
//}
//
//Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)
//
//not_run: try {
//	String newpassword = Mobile.getText(findTestObject('C2SC_CommonObject/txt_SetNewPassword'), GlobalVariable.intWaitTime)
//
//	println(newpassword)
//
//	if (!(newpassword.equals(null))) {
//		Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_NewPassword'), GlobalVariable.intWaitTime)
//
//		Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_NewPassword'), GlobalVariable.intWaitTime, 0)
//
//		Mobile.setText(findTestObject('C2SC_CommonObject/input_NewPassword'), 'Mindtree@1234', GlobalVariable.intWaitTime)
//
//		if (AndroidDevice.equals('iOS')) {
//			Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
//		}
//		
//		Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), GlobalVariable.intWaitTime)
//
//		Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), GlobalVariable.intWaitTime, 0)
//
//		Mobile.setText(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), 'Mindtree@1234', GlobalVariable.intWaitTime)
//
//		if (AndroidDevice.equals('iOS')) {
//			Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
//		}
//		
//		Mobile.tap(findTestObject('C2SC_CommonObject/btn_Continue'), GlobalVariable.intWaitTime)
//	}
//}
//catch (Exception e) {
//}
//
//Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)
//
//Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)
//
//Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)
//
//Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)
//
//Mobile.scrollToText('Set up Profile')
//
//Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)
//
//Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)
//
//Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)
//
//Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)
//
//Mobile.waitForElementPresent(findTestObject('C2SC_TellUsLittleAboutYouScreen/btn_Skip'), GlobalVariable.intWaitTime)
//
//Mobile.tap(findTestObject('C2SC_TellUsLittleAboutYouScreen/btn_Skip'), GlobalVariable.intWaitTime)
//
//not_run: Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_FirsttimeHomeBuyer'), GlobalVariable.intWaitTime)
//
//not_run: Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_FirsttimeHomeBuyer'), GlobalVariable.intWaitTime)
//
//not_run: Mobile.tap(findTestObject('C2SC_MyProfilePage/btn_Next'), GlobalVariable.intWaitTime)
//
//if (AndroidDevice.equals('Android')) {
//	try {
//		Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime,FailureHandling.STOP_ON_FAILURE)
//
//		Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime,FailureHandling.STOP_ON_FAILURE)
//
//		Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime,FailureHandling.STOP_ON_FAILURE)
//
//		Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime,FailureHandling.STOP_ON_FAILURE)
//	}
//	catch (Exception e) {
//	}
//}
WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin_Seller'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_MyProfilePage/tab_MyProfile'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_Home'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_TodoScreen/txt_ProvideYourWireInstructiontoTitleCompany'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_TodoScreen/txt_ProvideYourWireInstructiontoTitleCompany'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_TodoScreen/txt_ConfirmProvideYourWireInstructionstoTitleCompany'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_TodoScreen/btn_MarkAsDone'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_AddToDoItemScreen/tab_ToDo'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_AddToDoItemScreen/tab_Timeline'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_TodoScreen/txt_ProvideYourWireInstructiontoTitleCompany'), GlobalVariable.intWaitTime)

Mobile.closeApplication()





