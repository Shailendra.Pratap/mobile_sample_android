import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_TodoScreen/C2SC_Verify the fields in reschedule closing screen when user selects Rescheduled'), 
    [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('C2SC_ClosingFlow/txt_DueDate'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ClosingFlow/txt_Date'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ClosingFlow/txt_Date'), GlobalVariable.intWaitTime)

String RescheduledDate=Mobile.getText(findTestObject('C2SC_ClosingFlow/txt_Date'), GlobalVariable.intWaitTime)

println(RescheduledDate)

Mobile.waitForElementPresent(findTestObject('C2SC_ClosingFlow/btn_OK'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ClosingFlow/btn_OK'), GlobalVariable.intWaitTime)

Mobile.delay(5)

Mobile.tap(findTestObject('C2SC_ClosingFlow/txt_Time'), GlobalVariable.intWaitTime)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('iOS')) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}
else {
Mobile.tap(findTestObject('C2SC_ClosingFlow/btn_clockOK'),GlobalVariable.intWaitTime)
}
Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_ClosingFlow/btn_Add'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_ClosingFlow/btn_Add'), GlobalVariable.intWaitTime)

Mobile.delay(10)

String date = Mobile.getText(findTestObject('Object Repository/C2SC_ClosingFlow/txt_ClosingDate'), GlobalVariable.intWaitTime)

String result = date.contains(RescheduledDate)

result.equals(true)
