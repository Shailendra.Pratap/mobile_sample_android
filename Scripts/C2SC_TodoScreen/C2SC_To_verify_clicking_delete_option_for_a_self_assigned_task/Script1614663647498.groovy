import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW

Mobile.callTestCase(findTestCase('C2SC_TodoScreen/C2SC_Verify_buyer_able_to_add_the_task_when_enter_data_all_input_fields'),[:], FailureHandling.STOP_ON_FAILURE)

Mobile.scrollToText(GlobalVariable.toDoTask)

Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/txt_ToDoTitle'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/txt_ToDoTitle'),GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/input_TitleUpd'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_TodoScreen/btn_Edit'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_TodoScreen/btn_Done'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_TodoScreen/btn_Delete'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_TodoScreen/btn_Delete'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_TodoScreen/btn_Delete'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_TodoScreen/txt_deleteConfirmation'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_TodoScreen/txt_deleteConfirmation'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_TodoScreen/btn_DeleteConfirmationPopUp'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_AddToDoItemScreen/tab_ToDo'), GlobalVariable.intWaitTime)

Mobile.delay(5)

Mobile.verifyElementNotExist(findTestObject('C2SC_AddToDoItemScreen/txt_ToDoTitle'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

