import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_Homepage/C2SC_Verify_that_title_Schedule_Open_House_is_displayed_in_To_Do_page'), 
    [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('C2SC_HomePage/txt_Schedule Open House'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_HomePage/txt_ScheduleOpenHouseHeader'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_HomePage/btn_SOHAdd'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_HomePage/txt_SOHDateError'), GlobalVariable.intWaitTime)

Mobile.closeApplication()