import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.callTestCase(findTestCase('C2SC-3634_5566/C630857_C2SC_Verify_that_Note_to_Buyer_is_appearing_in_to_do'),[:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Object Repository/C2SC_HomePage/txt_NoteToBuyer'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_HomePage/title_NoteToBuyer'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_HomePage/input_Note'),GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_HomePage/input_Note'),'homeplace',GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_HomePage/btn_AddNotePlusSign'),GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_HomePage/input_Note'),'homeplace1',GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_HomePage/btn_AddNotePlusSign'),GlobalVariable.intWaitTime)

Mobile.verifyElementText(findTestObject('Object Repository/C2SC_HomePage/txt_AddedNoteInList'), 'homeplace')

Mobile.verifyElementText(findTestObject('Object Repository/C2SC_HomePage/txt_AddedNoteInList'), 'homeplace1')

Mobile.closeApplication()
