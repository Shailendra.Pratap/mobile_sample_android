import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

lstReturned = WebUI.callTestCase(findTestCase('C2SC_API_Save_Invitation/C2SC_Get_Invitation_ClosingDayPast'), [('requestBodyPOST') : requestBodyPOST], 
    FailureHandling.STOP_ON_FAILURE)

String sCollectionName = 'consumer-registration'

String sQueryColumnName = 'consumerUserId'

String sQueryColumnValue = lstReturned['emailId']

String sPhoneNumber = lstReturned['phonenumber']

String sGetColumnValue = 'invitationCode'

String sTransactionN = 'transaction'

String stransColumnName = 'transactionId'

String stransColumnValue = lstReturned['phonenumber']

String sValueupdateStatus = CustomKeywords.'com.database.DataBaseConnection.UpdateDetailsDb'(sCollectionName, sQueryColumnName, 
    sQueryColumnValue)

String sValueupdatetransStatus = CustomKeywords.'com.database.DataBaseConnection.UpdateDetailsDbStatus'(sTransactionN, stransColumnName, 
    stransColumnValue)

WebUI.delay(10)

Mobile.startApplication(GlobalVariable.sAppID, true)

WebUI.delay(10)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('iOS')) {
    //Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

    //Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
}

Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_Mobile'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_CommonObject/input_Mobile'), sPhoneNumber, GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

Mobile.hideKeyboard()

if (AndroidDevice.equals('iOS')) {
    Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.tap(findTestObject('C2SC_CommonObject/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

String sValueInvitationCode = CustomKeywords.'com.database.DataBaseConnection.getDetailsFromDb'(sCollectionName, sQueryColumnName, 
    sQueryColumnValue, sGetColumnValue)

println(sValueInvitationCode)

String s1 = sValueInvitationCode

char[] ch = s1.toCharArray()

Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_1'), String.valueOf(ch[0]), GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_2'), String.valueOf(ch[1]), GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_3'), String.valueOf(ch[2]), GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_4'), String.valueOf(ch[3]), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

try {
    //Mobile.getText(findTestObject('C2SC_CommonObject/txt_SetNewPassword'), GlobalVariable.sPassword)
    String newpassword = GlobalVariable.sPassword

    println(newpassword)

    if (!(newpassword.equals(null))) {
        WebUI.delay(10)

        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_NewPassword'), GlobalVariable.intWaitTime)

        Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_NewPassword'), GlobalVariable.intWaitTime, 0)

        Mobile.setText(findTestObject('C2SC_CommonObject/input_NewPassword'), 'Mindtree@1234', GlobalVariable.intWaitTime)

        WebUI.delay(5)

        if (AndroidDevice.equals('iOS')) {
            Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
        }
        
        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), GlobalVariable.intWaitTime)

        Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), GlobalVariable.intWaitTime, 0)

        Mobile.setText(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), 'Mindtree@1234', GlobalVariable.intWaitTime)

        WebUI.delay(5)

        if (AndroidDevice.equals('iOS')) {
            Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
        }
        
        WebUI.delay(10)

        Mobile.tapAndHold(findTestObject('C2SC_CommonObject/btn_Continue'), GlobalVariable.intWaitTime, 0)
    }
}
catch (Exception e) {

} 
Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)
Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Set up Profile')

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_TellUsLittleAboutYouScreen/btn_Skip'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_TellUsLittleAboutYouScreen/btn_Skip'), GlobalVariable.intWaitTime)

//Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_OKPopup'), GlobalVariable.intWaitTime)
//Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_OKPopup'), GlobalVariable.intWaitTime)
not_run: Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_FirsttimeHomeBuyer'), GlobalVariable.intWaitTime)

not_run: Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_FirsttimeHomeBuyer'), GlobalVariable.intWaitTime)

not_run: Mobile.tap(findTestObject('C2SC_MyProfilePage/btn_Next'), GlobalVariable.intWaitTime)

if (AndroidDevice.equals('Android')) {
	try {
		Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

		Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

		Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

		Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
	}
	catch (Exception e) {
	}
}

Mobile.waitForElementPresent(findTestObject('C2SC_MyProfilePage/tab_MyProfile'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_Home'), GlobalVariable.intWaitTime)

Mobile.delay(10)
String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('iOS')) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)
}

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_CompletedTransaction/txt_ClosingDetails'), GlobalVariable.intWaitTime)

//Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_CompletedTransaction/txt_ClosingDate'), GlobalVariable.intWaitTime)
//Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_CompletedTransaction/txt_ClosingTime'), GlobalVariable.intWaitTime)
//Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_CompletedTransaction/txt_ClosingTitleCompany'), GlobalVariable.intWaitTime)
Mobile.verifyElementExist(findTestObject('C2SC_ClosingDetailsInHomepage/txt_Date'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_ClosingDetailsInHomepage/txt_Time'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_ClosingDetailsInHomepage/txt_Title_Company'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_CompletedTransaction/txt_ClosingDaySubtitle'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_CompletedTransaction/btn_TransactionCompleted'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_CompletedTransaction/btn_TransactionCompleted'), GlobalVariable.intWaitTime)

Mobile.delay(5)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_CompletedTransaction/txt_congratulationsOnPurchase'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_CompletedTransaction/Img_RatingBar'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_CompletedTransaction/btn_RatingSubmit'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_CompletedTransaction/btn_RatingSubmit'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_CompletedTransaction/txt_HowWeCanHelp'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_CompletedTransaction/txt_agentIsHereToAssist'), GlobalVariable.intWaitTime)