import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.callTestCase(findTestCase('Test Cases/C2SC_Homepage/C2SC_Verify_the_appearance_of_obtain_homeowners_insurance_and_closing_documents_in_to_do_task'), 
    [:], FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_HomePage/txt_HOIToDo'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_HomePage/txt_HOIToDo'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_HomePage/txt_HOI_Header'), GlobalVariable.intWaitTime)

//Mobile.verifyElementText(findTestObject('Object Repository/C2SC_HomePage/txt_HOI_Header'), 'Update Homeowners Insurance')
Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_HomePage/btn_MarkAsDone'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_HomePage/Img_HOIclose Icon'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

