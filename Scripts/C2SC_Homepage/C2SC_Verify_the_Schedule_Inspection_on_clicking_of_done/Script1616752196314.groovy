import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_HomePageFlow'), [:], FailureHandling.STOP_ON_FAILURE)
String AndroidDevice = GlobalVariable.sDeviceAndroid
if (AndroidDevice.equals('iOS')) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)
}
Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_Home'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Schedule Inspection')

Mobile.waitForElementPresent(findTestObject('C2SC_HomePage/txt_ScheduleInspection'), GlobalVariable.intWaitTime)

Mobile.getText(findTestObject('C2SC_HomePage/txt_ScheduleInspection'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_HomePage/txt_ScheduleInspection'), GlobalVariable.intWaitTime)

Mobile.getText(findTestObject('C2SC_HomePage/txt_ScheduleInspection'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_HomePage/input_Company'), GlobalVariable.intWaitTime)

//Mobile.tapAndHold(findTestObject('C2SC_HomePage/input_Company'), GlobalVariable.intWaitTime, 0)
//
//Mobile.setText(findTestObject('C2SC_HomePage/input_Company'), 'TestC2SC', GlobalVariable.intWaitTime)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('Android')) {
    Mobile.tap(findTestObject('C2SC_HomePage/input_Duedate'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/icon_CalendarArrow'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/txt_CalendarDate'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/btn_Calendar_OK'), GlobalVariable.intWaitTime)
} else {
    Mobile.tap(findTestObject('Object Repository/C2SC_AddTeamMemberScreen/btn_Done'), GlobalVariable.intWaitTime)

    Mobile.tapAndHold(findTestObject('C2SC_AddToDoItemScreen/Img_CalendarIcon'), GlobalVariable.intWaitTime, 0)

    Mobile.swipe(170, 640, 170, 525)

    Mobile.swipe(170, 640, 170, 525)

    Mobile.swipe(170, 640, 170, 490)

    Mobile.swipe(170, 640, 170, 490)

    Mobile.tapAndHold(findTestObject('C2SC_AddToDoItemScreen/txt_CalendarDate'), GlobalVariable.intWaitTime, 0)
}

Mobile.waitForElementPresent(findTestObject('C2SC_HomePage/input_Time'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_HomePage/input_Time'), GlobalVariable.intWaitTime, 0)

//As the company field is changed now, it became a dropfdown and the feature is still in work for sprint7.Later will add the company
//Mobile.setText(findTestObject('C2SC_HomePage/input_Time'), '10:00 AM', GlobalVariable.intWaitTime)
//
//Mobile.waitForElementPresent(findTestObject('C2SC_HomePage/btn_Add'), GlobalVariable.intWaitTime)
//
//Mobile.tap(findTestObject('C2SC_HomePage/btn_Add'), GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)
//
//Mobile.waitForElementPresent(findTestObject('C2SC_HomePage/txt_Timeline'), GlobalVariable.intWaitTime)
//
//Mobile.tap(findTestObject('C2SC_HomePage/txt_Timeline'), GlobalVariable.intWaitTime)
//
//Mobile.waitForElementPresent(findTestObject('C2SC_HomePage/txt_ScheduleInspection'), GlobalVariable.intWaitTime)
//
//Mobile.getText(findTestObject('C2SC_HomePage/txt_ScheduleInspection'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

