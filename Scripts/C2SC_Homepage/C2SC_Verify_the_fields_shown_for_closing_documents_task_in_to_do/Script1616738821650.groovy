import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import org.openqa.selenium.Keys as Keys

//lstReturned = WebUI.callTestCase(findTestCase('C2SC_API_Save_Invitation/C2SC_Get_Invitation_ClosingDayToday'), [('lstReturned') : [:], ('requestBodyPOST') : '{\r\n  "sourceApp": "MyDeals",\r\n  "transactionId": "Tomtom11",\r\n  "officeId" :"2",\r\n    \r\n  "contractDate": "2021-04-04T11:23:48Z",\r\n  "estimatedClosingDate":"2021-08-04T11:23:48Z",\r\n  "agent": [\r\n    {\r\n      "agentOktaId": "00urwnlnzjdsnfxQl0h7",\r\n      "agentTridentId": "TestAgent2OktaId"\r\n    }\r\n  ],\r\n  "property": {\r\n    "mlsID": "123",\r\n    "addressLine1": "street1",\r\n    "addressLine2": "lane 4",\r\n    "city": "seattle",\r\n    "state": "Texas",\r\n    "zip": "98101"\r\n  },\r\n  "consumer": [\r\n    {\r\n        \r\n      "firstName": "geetanjali",\r\n      "lastName": "gavi",\r\n      "emailId": "test@test.com",\r\n      "phoneNo": "9898989898",\r\n      "country": "US"\r\n    }\r\n  ]\r\n}'], FailureHandling.STOP_ON_FAILURE)

lstReturned = WebUI.callTestCase(findTestCase('C2SC_API_Save_Invitation/C2SC_Get_Invitation_ClosingDayToday'), [('requestBodyPOST') : requestBodyPOST],
	FailureHandling.STOP_ON_FAILURE)
String sCollectionName = 'consumer-registration'

String sQueryColumnName = 'consumerUserId'

String sQueryColumnValue = lstReturned['emailId']

String sPhoneNumber = lstReturned['phonenumber']

String sGetColumnValue = 'invitationCode'

String sTransactionN = 'transaction'

String stransColumnName = 'transactionId'

String stransColumnValue = lstReturned['phonenumber']

String sValueupdateStatus = CustomKeywords.'com.database.DataBaseConnection.UpdateDetailsDb'(sCollectionName, sQueryColumnName, 
    sQueryColumnValue)

String sValueupdatetransStatus = CustomKeywords.'com.database.DataBaseConnection.UpdateDetailsDbStatus'(sTransactionN, stransColumnName, 
    stransColumnValue)

WebUI.delay(10)

Mobile.startApplication(GlobalVariable.sAppID, true)

WebUI.delay(10)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('iOS')) {
    //Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

    //Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
}

Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_Mobile'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_CommonObject/input_Mobile'), sPhoneNumber, GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

Mobile.hideKeyboard()

if (AndroidDevice.equals('iOS')) {
    Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.tap(findTestObject('C2SC_CommonObject/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

String sValueInvitationCode = CustomKeywords.'com.database.DataBaseConnection.getDetailsFromDb'(sCollectionName, sQueryColumnName, 
    sQueryColumnValue, sGetColumnValue)

println(sValueInvitationCode)

String s1 = sValueInvitationCode

char[] ch = s1.toCharArray()

Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_1'), String.valueOf(ch[0]), GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_2'), String.valueOf(ch[1]), GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_3'), String.valueOf(ch[2]), GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_4'), String.valueOf(ch[3]), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

try {
    //Mobile.getText(findTestObject('C2SC_CommonObject/txt_SetNewPassword'), GlobalVariable.sPassword)
    String newpassword = GlobalVariable.sPassword

    println(newpassword)

    if (!(newpassword.equals(null))) {
        WebUI.delay(10)

        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_NewPassword'), GlobalVariable.intWaitTime)

        Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_NewPassword'), GlobalVariable.intWaitTime, 0)

        Mobile.setText(findTestObject('C2SC_CommonObject/input_NewPassword'), 'Mindtree@1234', GlobalVariable.intWaitTime)

        WebUI.delay(5)

        if (AndroidDevice.equals('iOS')) {
            Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
        }
        
        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), GlobalVariable.intWaitTime)

        Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), GlobalVariable.intWaitTime, 0)

        Mobile.setText(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), 'Mindtree@1234', GlobalVariable.intWaitTime)

        WebUI.delay(5)

        if (AndroidDevice.equals('iOS')) {
            Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
        }
        
        WebUI.delay(10)

        Mobile.tapAndHold(findTestObject('C2SC_CommonObject/btn_Continue'), GlobalVariable.intWaitTime, 0)
    }
}
catch (Exception e) {

} 

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Set up Profile')

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_TellUsLittleAboutYouScreen/btn_Skip'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_TellUsLittleAboutYouScreen/btn_Skip'), GlobalVariable.intWaitTime)

//Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_OKPopup'), GlobalVariable.intWaitTime)
//Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_OKPopup'), GlobalVariable.intWaitTime)
not_run: Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_FirsttimeHomeBuyer'), GlobalVariable.intWaitTime)

not_run: Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_FirsttimeHomeBuyer'), GlobalVariable.intWaitTime)

not_run: Mobile.tap(findTestObject('C2SC_MyProfilePage/btn_Next'), GlobalVariable.intWaitTime)

if (AndroidDevice.equals('Android')) {
	try {
		Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

		Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

		Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

		Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
	}
	catch (Exception e) {
	}
}

Mobile.delay(10)
if (AndroidDevice.equals('iOS')) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)
}
Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_MyTeam'), GlobalVariable.intWaitTime)

if(Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/btn_OK'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('C2SC_GraSsnParameter/btn_OK'), GlobalVariable.intWaitTime)
}

Mobile.tap(findTestObject('C2SC_MyTeamPage/txt_TitleClosing'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_MyTeamPage/btn_Edit'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)

if (Mobile.verifyElementExist(findTestObject('C2SC_MyTeamPage/btn_Edit'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('C2SC_MyTeamPage/btn_Edit'), GlobalVariable.intWaitTime)
}

Mobile.verifyElementExist(findTestObject('C2SC_MyTeamPage/txt_WhoWouldYouLikeToAdd'), GlobalVariable.intWaitTime)

String CompanyName = 'Title Agency'

Mobile.tapAndHold(findTestObject('C2SC_MyTeamPage/input_CompanyName'), 2, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_MyTeamPage/input_CompanyName'), CompanyName, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), 'Jack', GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_MyTeamPage/input_Email'),2,GlobalVariable.intWaitTime)
Mobile.setText(findTestObject('C2SC_MyTeamPage/input_Email'), 'Jack.Ryan@test.com', GlobalVariable.intWaitTime)
Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)
Mobile.tapAndHold(findTestObject('C2SC_MyTeamPage/input_PhoneNumber'), 2, GlobalVariable.intWaitTime)
Mobile.setText(findTestObject('C2SC_MyTeamPage/input_PhoneNumber'), '1234512345', GlobalVariable.intWaitTime)

if (Mobile.verifyElementExist(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
    Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

//Mobile.swipe(145, 662, 145, 74)

Mobile.tap(findTestObject('C2SC_MyTeamPage/btn_AddSPPlusIcon'), GlobalVariable.intWaitTime)

Mobile.delay(5)

Mobile.verifyElementExist(findTestObject('C2SC_MyTeamPage/txt_TitleClosing'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_Home'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_AddToDoItemScreen/tab_ToDo'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Closing Documents Available')

Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_HomePage/txt_CDAToDo'), GlobalVariable.intWaitTime)

//Mobile.verifyElementText(findTestObject('Object Repository/C2SC_HomePage/txt_CDAToDo'), 'Final Closing Disclosure')//changed feature 

//Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_HomePage/txt_CDASubtitle'), GlobalVariable.intWaitTime)
Mobile.closeApplication()