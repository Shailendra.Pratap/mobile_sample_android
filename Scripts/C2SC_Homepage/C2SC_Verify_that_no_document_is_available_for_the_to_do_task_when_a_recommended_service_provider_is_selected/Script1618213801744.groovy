import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_HomePageFlow'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.delay(5)

Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_MyTeam'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyTeamPage/txt_HomeOwnersInsurance'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyTeamPage/btn_Edit'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_MyTeamPage/txt_WhoWouldYouLikeToAdd'), GlobalVariable.intWaitTime)

String CompanyName = 'Insurance Agency'

Mobile.tapAndHold(findTestObject('C2SC_MyTeamPage/input_CompanyName'), 2, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_MyTeamPage/input_CompanyName'), CompanyName, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), 'Jack', GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_MyTeamPage/input_Email'), 'Jack.Ryan@test.com', GlobalVariable.intWaitTime)

Mobile.hideKeyboard()

Mobile.tapAndHold(findTestObject('C2SC_MyTeamPage/input_PhoneNumber'), 2, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_MyTeamPage/input_PhoneNumber'), '1234512345', GlobalVariable.intWaitTime)

if (Mobile.verifyElementExist(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
    Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.swipe(145, 662, 145, 74)

Mobile.tap(findTestObject('C2SC_MyTeamPage/btn_AddSPPlusIcon'), GlobalVariable.intWaitTime)

Mobile.delay(5)

Mobile.verifyElementExist(findTestObject('C2SC_MyTeamPage/txt_HomeOwnersInsurance'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_Home'), GlobalVariable.intWaitTime)

Mobile.delay(10)
String AndroidDevice = GlobalVariable.sDeviceAndroid
if (AndroidDevice.equals('iOS')) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)
}

Mobile.scrollToText('Obtain Homeowners Insurance')

Mobile.verifyElementText(findTestObject('Object Repository/C2SC_HomePage/txt_HOIToDo'), 'Obtain Homeowners Insurance')

Mobile.verifyElementText(findTestObject('Object Repository/C2SC_HomePage/txt_HOISubtitle'), 'Check out our recommended provider in the My Team section of the app to obtain a quote')

Mobile.tap(findTestObject('Object Repository/C2SC_HomePage/txt_HOIToDo'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_HomePage/btn_MarkAsDone'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

