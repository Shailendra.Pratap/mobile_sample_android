import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('bs://0b3736349dfb90a04ce6d6b1b697ce47befd07cd', true)

Mobile.getText(findTestObject('new_login_object/txt_TermsofUse'), 0)

Mobile.getText(findTestObject('Object Repository/new_login_object/XCUIElementTypeStaticText - Privacy Policy'), 0)

Mobile.getText(findTestObject('null'), 0)

Mobile.getText(findTestObject('null'), 0)

Mobile.tap(findTestObject(''), 0)

Mobile.getText(findTestObject(''), 0)

Mobile.tap(findTestObject(''), 0)

Mobile.tap(findTestObject(''), 0)

Mobile.getText(findTestObject('Object Repository/new_login_object/XCUIElementTypeStaticText - Terms of Use (1)'), 0)

Mobile.tap(findTestObject('Object Repository/new_login_object/XCUIElementTypeButton - terms_btn_back'), 0)

Mobile.tap(findTestObject(''), 0)

Mobile.getText(findTestObject('new_login_object/txt_PrivacyNotice'), 0)

Mobile.tap(findTestObject('Object Repository/new_login_object/XCUIElementTypeButton - privacyPolicy_btn_back'), 0)

Mobile.closeApplication()

