import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_Login'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/txt_SellerYourInformation'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Set up Profile')

Mobile.verifyElementExist(findTestObject('C2SC_ProfileSetup/txt_YouAreSelling'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_ProfileSetup/txt_Street'), GlobalVariable.intWaitTime)

verifyStreetEditable = Mobile.getAttribute(findTestObject('Object Repository/C2SC_ProfileSetup/input_Street'), 'enabled', GlobalVariable.intWaitTime)

if(verifyStreetEditable.toString().equalsIgnoreCase("false")) {
	print("Street is non-editable")
}

Mobile.verifyElementExist(findTestObject('C2SC_ProfileSetup/txt_City'), GlobalVariable.intWaitTime)

verifyCityEditable = Mobile.getAttribute(findTestObject('C2SC_ProfileSetup/input_City'), 'enabled', GlobalVariable.intWaitTime)

if(verifyCityEditable.toString().equalsIgnoreCase("false")) {
	print("City is non-editable")
}

Mobile.verifyElementExist(findTestObject('C2SC_ProfileSetup/txt_State'), GlobalVariable.intWaitTime)

verifyStateEditable = Mobile.getAttribute(findTestObject('C2SC_ProfileSetup/input_State'), 'enabled', GlobalVariable.intWaitTime)

if(verifyStateEditable.toString().equalsIgnoreCase("false")) {
	print("State is non-editable")
}

Mobile.verifyElementExist(findTestObject('C2SC_ProfileSetup/txt_ZipCode'), GlobalVariable.intWaitTime)

verifyZipEditable = Mobile.verifyElementExist(findTestObject('C2SC_ProfileSetup/input_ZipCode'), GlobalVariable.intWaitTime)

if(verifyZipEditable.toString().equalsIgnoreCase("false")) {
	print("Zip is non-editable")
}

Mobile.closeApplication()