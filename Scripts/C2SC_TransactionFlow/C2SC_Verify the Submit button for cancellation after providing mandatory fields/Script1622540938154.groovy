import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

// WebUI.callTestCase(findTestCase('null'), [:], FailureHandling.STOP_ON_FAILURE)
lstReturned = WebUI.callTestCase(findTestCase('C2SC_API_Save_Invitation/C2SC_Get_Invitation_ClosingDayYesterday'), [('requestBodyPOST') : '{\r\n    "sourceApp": "MyDeals",\r\n    "transactionId": "9899111122",\r\n    "requestTime": "2021-05-20",\r\n    "officeId": "99007839",\r\n    "estimatedClosingDate": "2021-05-25",\r\n    "side": "sell",\r\n    "contractDate": "2021-05-20",\r\n    "listingAgreementDate": "2021-05-20",\r\n    "agent": [\r\n        {\r\n            "agentOktaId": "00urwnkh6p6TfkovG0h7",\r\n            "agentTridentId": "TestAgent2OktaId"\r\n        }\r\n    ],\r\n    "property": {\r\n        "mlsID": "123",\r\n        "addressLine1": "street1",\r\n        "addressLine2": "lane 4",\r\n        "city": "Sarasota",\r\n        "state": "FL",\r\n        "zip": "98101"\r\n    },\r\n    "consumer": [\r\n        {\r\n            "firstName": "gokul",\r\n            "lastName": "qa",\r\n            "emailId": "gokul10@yopmail.com",\r\n            "phoneNo": "9742766425",\r\n            "country": "IN"\r\n        }\r\n    ]\r\n}'
        , ('emailId') : '', ('lstReturn') : [:]], FailureHandling.STOP_ON_FAILURE)

//println(lstReturned)
String sCollectionName = 'consumer-registration'

String sQueryColumnName = 'consumerUserId'

String sQueryColumnValue = lstReturned['emailId']

String sPhoneNumber = lstReturned['phonenumber']

String sGetColumnValue = 'invitationCode'

String sValueupdateStatus = CustomKeywords.'com.database.DataBaseConnection.UpdateDetailsDb'(sCollectionName, sQueryColumnName, 
    sQueryColumnValue)

String sValueInvitationCode = CustomKeywords.'com.database.DataBaseConnection.getDetailsFromDb'(sCollectionName, sQueryColumnName, 
    sQueryColumnValue, sGetColumnValue)

println(sValueInvitationCode)

String s1 = sValueInvitationCode

char[] ch = s1.toCharArray()

WebUI.delay(10)

Mobile.startApplication(GlobalVariable.sAppID, true)

WebUI.delay(10)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('iOS')) {
    Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
}

Mobile.setText(findTestObject('C2SC_CommonObject/input_Mobile'), sPhoneNumber, GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

if (AndroidDevice.equals('iOS')) {
    Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.scrollToText('Terms of Use', FailureHandling.STOP_ON_FAILURE)

Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_1'), String.valueOf(ch[0]), GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_2'), String.valueOf(ch[1]), GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_3'), String.valueOf(ch[2]), GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_CommonObject/input_InvitationCode_4'), String.valueOf(ch[3]), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

try {
    //Mobile.getText(findTestObject('C2SC_CommonObject/txt_SetNewPassword'), GlobalVariable.sPassword)
    String newpassword = GlobalVariable.sPassword

    println(newpassword)

    if (!(newpassword.equals(null))) {
        WebUI.delay(10)

        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_NewPassword'), GlobalVariable.intWaitTime)

        Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_NewPassword'), GlobalVariable.intWaitTime, 0)

        Mobile.setText(findTestObject('C2SC_CommonObject/input_NewPassword'), 'Mindtree@1234', GlobalVariable.intWaitTime)

        WebUI.delay(5)

        if (AndroidDevice.equals('iOS')) {
            Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
        }
        
        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), GlobalVariable.intWaitTime)

        Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), GlobalVariable.intWaitTime, 0)

        Mobile.setText(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), 'Mindtree@1234', GlobalVariable.intWaitTime)

        WebUI.delay(5)

        if (AndroidDevice.equals('iOS')) {
            Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
        }
        
        WebUI.delay(10)

        Mobile.tapAndHold(findTestObject('C2SC_CommonObject/btn_Continue'), GlobalVariable.intWaitTime, 0)
    }
}
catch (Exception e) {
} 

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

not_run: try {
    String newpassword = Mobile.getText(findTestObject('C2SC_CommonObject/txt_SetNewPassword'), GlobalVariable.intWaitTime)

    println(newpassword)

    if (!(newpassword.equals(null))) {
        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_NewPassword'), GlobalVariable.intWaitTime)

        Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_NewPassword'), GlobalVariable.intWaitTime, 0)

        Mobile.setText(findTestObject('C2SC_CommonObject/input_NewPassword'), 'Mindtree@1234', GlobalVariable.intWaitTime)

        if (AndroidDevice.equals('iOS')) {
            Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
        }
        
        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), GlobalVariable.intWaitTime)

        Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), GlobalVariable.intWaitTime, 0)

        Mobile.setText(findTestObject('C2SC_CommonObject/input_ConfirmPassword'), 'Mindtree@1234', GlobalVariable.intWaitTime)

        if (AndroidDevice.equals('iOS')) {
            Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
        }
        
        Mobile.tap(findTestObject('C2SC_CommonObject/btn_Continue'), GlobalVariable.intWaitTime)
    }
}
catch (Exception e) {
} 

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Set up Profile')

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_TellUsLittleAboutYouScreen/btn_Skip'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_TellUsLittleAboutYouScreen/btn_Skip'), GlobalVariable.intWaitTime)

//Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_OKPopup'), GlobalVariable.intWaitTime)
//Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_OKPopup'), GlobalVariable.intWaitTime)
not_run: Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_FirsttimeHomeBuyer'), GlobalVariable.intWaitTime)

not_run: Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_FirsttimeHomeBuyer'), GlobalVariable.intWaitTime)

not_run: Mobile.tap(findTestObject('C2SC_MyProfilePage/btn_Next'), GlobalVariable.intWaitTime)

if (AndroidDevice.equals('Android')) {
    try {
        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

        Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

        Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
    }
    catch (Exception e) {
    } 
}

Mobile.waitForElementPresent(findTestObject('C2SC_MyProfilePage/tab_MyProfile'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_Home'), GlobalVariable.intWaitTime)

Mobile.delay(15, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_ClosingFlow/btn_SomethingWentWrong'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ClosingFlow/btn_SomethingWentWrong'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ClosingFlow/btn_Cancelled'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ClosingFlow/btn_Cancelled'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Submit')

Mobile.verifyElementExist(findTestObject('C2SC_ClosingFlow/txt_Description'), GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_ClosingFlow/txt_Description'), 'This is description', GlobalVariable.intWaitTime)

Mobile.hideKeyboard()										

Mobile.verifyElementExist(findTestObject('C2SC_ClosingFlow/StarRating'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ClosingFlow/StarRating'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ClosingFlow/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.delay(10)

Mobile.scrollToText('Cancelled')

Mobile.verifyElementExist(findTestObject('C2SC_ClosingFlow/txt_Cancelled'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_ClosingDetailsInHomepage/txt_Date'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_ClosingDetailsInHomepage/txt_Time'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_ClosingDetailsInHomepage/txt_Title_Company'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

