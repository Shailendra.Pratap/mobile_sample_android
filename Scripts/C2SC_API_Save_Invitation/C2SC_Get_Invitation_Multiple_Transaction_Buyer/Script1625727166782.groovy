import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import java.sql.*
import com.kms.katalon.core.testobject.RequestObject as RequestObject
import com.kms.katalon.core.testobject.impl.HttpTextBodyContent as HttpTextBodyContent
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import groovy.json.JsonBuilder as JsonBuilder
import groovy.json.JsonSlurper as JsonSlurper
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint

//Getting Global Varibales
lstReturned = WebUI.callTestCase(findTestCase('C2SC_API_Save_Invitation/C2SC_Get_Invitation'), [('requestBodyPOST') : requestBodyPOST], 
    FailureHandling.STOP_ON_FAILURE)

String sCollectionName = 'consumer-registration'

String sQueryColumnName = 'consumerUserId'

String sQueryColumnValue = lstReturned['emailId']

String sPhoneNumber = lstReturned['phonenumber']

String sGetColumnValue = 'invitationCode'

String sTransactionN = 'transaction'

String stransColumnName = 'transactionId'

String stransColumnValue = lstReturned['phonenumber']

String sValueupdateStatus = CustomKeywords.'com.database.DataBaseConnection.UpdateDetailsDb'(sCollectionName, sQueryColumnName,
	sQueryColumnValue)

String sValueupdatetransStatus = CustomKeywords.'com.database.DataBaseConnection.UpdateDetailsDbStatus'(sTransactionN, stransColumnName,
	stransColumnValue)

println(emailId);

def endpoint = GlobalVariable.ExpEndPoint

def responseFilePath = GlobalVariable.sJSONResultPath

def testCaseName = GlobalVariable.sTestCaseName

int num1

int num2

int num3

int set2

int set3

Random generator = new Random()

num1 = (generator.nextInt(7) + 1)

num2 = generator.nextInt(8)

num3 = generator.nextInt(8)

set2 = (generator.nextInt(643) + 100)

set3 = (generator.nextInt(8999) + 1000)

String phonenumber = (((((((num1 + '') + num2) + '') + num3) + '') + set2) + '') + set3

int randNum = ((Math.random() * 1000) as int)

println(phonenumber)

try {
    //creating the request body
    JsonSlurper slurper = new JsonSlurper()

    Map parsedJsonPOST = slurper.parseText(requestBodyPOST //getting request body from TCVariable
        )

    JsonBuilder builderPOST = new JsonBuilder(parsedJsonPOST)

    def emailId = GlobalVariable.sGlobalEmailID

    transactionId = phonenumber

    //Replacing required field's values
    builderPOST.content.transactionId = phonenumber

    //	builderPOST.content.firstName = "Rajen"
    //	builderPOST.content.lastName = "@123"
    builderPOST.content.consumer[0].emailId = emailId

    builderPOST.content.consumer[0].phoneNo = phonenumber

    String requestBodyPOST = builderPOST.toString( //Build the request
        )

    println(emailId)

    //creating request object(to pass the api request url)
    def requestObjectPOST = ((findTestObject('Object Repository/Common_API/Post')) as RequestObject)

    String resourcePOST = '/invitation' //creating resource url

    String fullRequestUrlPOST = endpoint + resourcePOST //creating full url to pass

    requestObjectPOST.setBodyContent(new HttpTextBodyContent(requestBodyPOST, 'UTF-8', 'application/json') //setting request body
        )

    requestObjectPOST.setRestUrl(fullRequestUrlPOST //setting the request url
        )

    println(requestBodyPOST)

    //sending the request
    def fullResponsePOST = WS.sendRequest(requestObjectPOST //sending the request and storing response
        )

    def textResponsePOST = fullResponsePOST.getResponseText() //Get the response in text format

    //storing the response
    String currentDateTime = CustomKeywords.'frameworkFunctions.UtilityLib.CurrentDateTime_UF'( //getting date for file nomenclature
        )

    String responseFileName = ((((responseFilePath + '/Response_') + testCaseName) + '_') + currentDateTime) + '.json' //defining res file path

    CustomKeywords.'frameworkFunctions.processJSON.writeToFile'(responseFileName, textResponsePOST //Writing to JSON File
        )

    //Verifying status code
    WS.verifyResponseStatusCode(fullResponsePOST, 200)

    //Validate response data here if required
    def response = new JsonSlurper().parseText(textResponsePOST)

    //println(response)
    //Return email Id
    lstReturn = [('emailId') : emailId, ('phonenumber') : phonenumber, ('transactionId') : transactionId]
	
	

    //println lstReturn
    return lstReturn
	
	
}
catch (Exception e) {
    KeywordUtil.markError(e.getMessage())
} 
finally { 
    CustomKeywords.'frameworkFunctions.DataExtraction.closeDatabaseConnection'()
}

