import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import java.text.SimpleDateFormat;
import com.kms.katalon.core.util.KeywordUtil

Mobile.startApplication(GlobalVariable.sAppID, true)

WebUI.delay(10)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (Mobile.verifyElementExist(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
}

Mobile.verifyElementExist(findTestObject('C2SC_LoginScreen/lnk_Login'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_LoginScreen/lnk_Login'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_LoginScreen/input_Email'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_LoginScreen/input_Email'), GlobalVariable.intWaitTime, 0)

Mobile.setText(findTestObject('C2SC_LoginScreen/input_Email'), GlobalVariable.sEmailIdTitle, GlobalVariable.intWaitTime)

try {
	Mobile.hideKeyboard()
}
catch(Exception e) {
	
}

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_LoginScreen/input_Password'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_LoginScreen/input_Password'), GlobalVariable.intWaitTime, 0)

Mobile.setText(findTestObject('C2SC_LoginScreen/input_Password'), 'Realogy1$', GlobalVariable.intWaitTime)

try {
	Mobile.hideKeyboard()
}
catch(Exception e) {
	
}

Mobile.waitForElementPresent(findTestObject('C2SC_LoginScreen/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_LoginScreen/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

if (Mobile.verifyElementExist(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
}

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_MyProfilePage/tab_MyDocs'), GlobalVariable.intWaitTime)

Mobile.delay(10)

Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_MyDocs'), GlobalVariable.intWaitTime)

Mobile.delay(10)

try {
	Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_MyDocsCounter/txt_Contract'), GlobalVariable.intWaitTime)
		}catch(Exception e) {
	Mobile.tap(findTestObject('Object Repository/C2CS_MyDoc/txt_Contract'), GlobalVariable.intWaitTime)
		}

Mobile.waitForElementPresent(findTestObject('Object Repository/C2CS_MyDoc/txt_Date'), GlobalVariable.intWaitTime)



String sdate= Mobile.getText(findTestObject('Object Repository/C2CS_MyDoc/txt_Date'), GlobalVariable.intWaitTime)
println(sdate)


String strDate='05/19/2021'

if(sdate.contains(strDate)) {
	KeywordUtil.markPassed("Passed")
}
else {
	KeywordUtil.markFailed("Failed")
}

Mobile.closeApplication()

