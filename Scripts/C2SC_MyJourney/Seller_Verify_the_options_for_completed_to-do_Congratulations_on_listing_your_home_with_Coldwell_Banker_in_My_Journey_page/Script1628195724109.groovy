import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_HomePageFlowSeller'), [:], FailureHandling.STOP_ON_FAILURE)


WebUI.callTestCase(findTestCase('Test Cases/C2SC_Common_Testcase/C2SC_EmailLogin_Seller'),[:],FailureHandling.STOP_ON_FAILURE)

Mobile.scrollToText('Congratulations on listing your home with Coldwell Banker!')

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_SellerToDo/lbl_Congratulations on listing your home with Coldwell Banker'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_SellerToDo/lbl_Congratulations on listing your home with Coldwell Banker'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_TodoScreen/btn_MarkAsDone'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_TodoScreen/btn_MarkAsDone'), GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_MyJourney/btn_MyJourney'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_MyJourney/btn_MyJourney'), GlobalVariable.intWaitTime)

Mobile.verifyElementText(findTestObject('C2SC_SellerToDo/txt_Congratulations on listing your home with Coldwell Banker'), '')

//Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_TodoScreen/Img_tickMarkSubmitYourInitialEarnestMoney'),GlobalVariable.intWaitTime)

Mobile.closeApplication()





























