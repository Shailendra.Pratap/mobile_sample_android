import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin'), [:], FailureHandling.STOP_ON_FAILURE)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('iOS')) {
    Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)
}

Mobile.tap(findTestObject('C2SC_MyAgentPage/icon_MyAgent'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_MyAgentPage/img_ProfilePic'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_MyAgentPage/txt_AgentName'), GlobalVariable.intWaitTime)

//Mobile.verifyElementExist(findTestObject('C2SC_MyAgentPage/txt_CompanyName'), GlobalVariable.intWaitTime)
Mobile.verifyElementExist(findTestObject('C2SC_MyAgentPage/txt_AgentMobileNumber'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_MyAgentPage/txt_MessageAgent'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_MyAgentPage/txt_AgentEmailId'), GlobalVariable.intWaitTime)

Mobile.closeApplication()