import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_Login_Seller_Multiple_Transaction'), [('lstReturned') : [:], ('requestBodyPOST') : '{\r\n    "sourceApp": "MyDeals",\r\n    "transactionId": "9899111122",\r\n    "requestTime": "2021-05-20",\r\n    "officeId": "99007839",\r\n    "estimatedClosingDate": "2021-05-25",\r\n    "side": "sell",\r\n    "contractDate": "2021-05-20",\r\n    "listingAgreementDate": "2021-05-20",\r\n    "agent": [\r\n        {\r\n            "agentOktaId": "00urwnkh6p6TfkovG0h7",\r\n            "agentTridentId": "TestAgent2OktaId"\r\n        }\r\n    ],\r\n    "property": {\r\n        "mlsID": "",\r\n        "addressLine1": "street1",\r\n        "addressLine2": "lane 4",\r\n        "city": "Sarasota",\r\n        "state": "FL",\r\n        "zip": "98101"\r\n    },\r\n    "consumer": [\r\n        {\r\n            "firstName": "gokul",\r\n            "lastName": "qa",\r\n            "emailId": "gokul10@yopmail.com",\r\n            "phoneNo": "9742766425",\r\n            "country": "US"\r\n        }\r\n    ]\r\n}'], 
    FailureHandling.STOP_ON_FAILURE)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('C2SC_MultipleTransaction/txt_buyingProperties'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_MultipleTransaction/txt_buyingPropertiesSubtitle'), GlobalVariable.intWaitTime)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('Android')) {
    try {
        Mobile.verifyElementVisible(findTestObject('C2SC_MultipleTransaction/btn_myListing'), GlobalVariable.intWaitTime)
    }
    catch (Exception e) {
    } 
}

def transactionAddress = Mobile.getText(findTestObject('C2SC_MultipleTransaction/txt_transactionAddress'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MultipleTransaction/txt_transactionAddress'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_MultipleTransaction/btn_transactionNext'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MultipleTransaction/btn_transactionNext'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Set up Profile')

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_TellUsLittleAboutYouScreen/btn_Skip'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_TellUsLittleAboutYouScreen/btn_Skip'), GlobalVariable.intWaitTime)

if (AndroidDevice.equals('Android')) {
    try {
        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

        Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

        Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
    }
    catch (Exception e) {
    } 
}

Mobile.waitForElementPresent(findTestObject('C2SC_MyProfilePage/tab_MyProfile'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_Home'), GlobalVariable.intWaitTime)

if (AndroidDevice.equals('iOS')) {
    Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
}

Mobile.verifyElementVisible(findTestObject('C2SC_MultipleTransaction/txt_transactionToggleHome'), GlobalVariable.intWaitTime)

def selectedTransactionAddress = Mobile.getText(findTestObject('C2SC_MultipleTransaction/txt_transactionToggleHome'), GlobalVariable.intWaitTime)

if (AndroidDevice.equals('Android')) {
    if (transactionAddress == selectedTransactionAddress) {
        println('Transaction address displayed correctly')
    } else {
        KeywordUtil.markFailed('Transaction address displayed does not match with selected address')
    }
}

Mobile.closeApplication()