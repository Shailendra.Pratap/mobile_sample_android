import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable


WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_Login_Buyer_Multiple_Transaction'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

String AndroidDevice = GlobalVariable.sDeviceAndroid

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Agent'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_Next'), GlobalVariable.intWaitTime)

Mobile.scrollToText('Set up Profile')

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_ProfileSetup/btn_SetUpProfile'), GlobalVariable.intWaitTime)

Mobile.delay(10, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_TellUsLittleAboutYouScreen/btn_Skip'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_TellUsLittleAboutYouScreen/btn_Skip'), GlobalVariable.intWaitTime)

if (AndroidDevice.equals('Android')) {
	try {
		Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

		Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

		Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

		Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
	}
	catch (Exception e) {
	}
}

//WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_MyProfilePage/tab_MyProfile'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_HomePage/txt_YouAreCurrentlyViewing'), GlobalVariable.intWaitTime)

Mobile.closeApplication()