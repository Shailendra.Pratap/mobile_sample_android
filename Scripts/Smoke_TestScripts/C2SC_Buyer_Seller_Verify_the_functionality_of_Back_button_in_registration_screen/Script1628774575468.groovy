import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


lstReturned = WebUI.callTestCase(findTestCase('C2SC_API_Save_Invitation/C2SC_Get_Invitation'), [('requestBodyPOST') : requestBodyPOST],
	FailureHandling.STOP_ON_FAILURE)

String sCollectionName = 'consumer-registration'

String sQueryColumnName = 'consumerUserId'

String sQueryColumnValue = lstReturned['emailId']

String sPhoneNumber = lstReturned['phonenumber']

String sGetColumnValue = 'invitationCode'

String sTransactionN = 'transaction'

String stransColumnName = 'transactionId'

String stransColumnValue = lstReturned['phonenumber']

String sValueupdateStatus = CustomKeywords.'com.database.DataBaseConnection.UpdateDetailsDb'(sCollectionName, sQueryColumnName,
	sQueryColumnValue)

String sValueupdatetransStatus = CustomKeywords.'com.database.DataBaseConnection.UpdateDetailsDbStatus'(sTransactionN, stransColumnName,
	stransColumnValue)

WebUI.delay(10)

Mobile.startApplication(GlobalVariable.sAppID, true)

WebUI.delay(10)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('iOS')) {
	Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
}

Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_Mobile'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_CommonObject/input_Mobile'), sPhoneNumber, GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

Mobile.hideKeyboard()

if (AndroidDevice.equals('iOS')) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.tap(findTestObject('C2SC_CommonObject/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_CommonObject/txt_prepopulatedPhoneNumber'),GlobalVariable.intWaitTime)

String prePopulatedMobileNumber=Mobile.getText(findTestObject('Object Repository/C2SC_CommonObject/txt_prepopulatedPhoneNumber'),GlobalVariable.intWaitTime)

println(prePopulatedMobileNumber)

boolean equalNumber=Mobile.verifyEqual(sPhoneNumber,prePopulatedMobileNumber)

println("RegisterNumber and Prepopulated Number Both are same"+""+equalNumber)



Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_LoginScreen/btn_Back'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_LoginScreen/btn_Back'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_LoginScreen/btn_LandingPageEmail'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

