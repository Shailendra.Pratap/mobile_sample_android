import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_MyJourney/btn_MyJourney'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_MyJourney/btn_MyJourney'), GlobalVariable.intWaitTime)

Mobile.verifyElementText(findTestObject('Object Repository/C2SC_MyJourney/txt_MyJournerTitle'), 'My Journey')

Mobile.verifyElementText(findTestObject('Object Repository/C2SC_MyJourney/txt_MyJourneySubtitle'), 'Here is a list of tasks you might need to complete')

//Mobile.delay(90)
Mobile.verifyElementText(findTestObject('Object Repository/C2SC_MyJourney/txt_ReviewYourContract'), 'Review your contract')

//Mobile.delay(90)
Mobile.verifyElementText(findTestObject('Object Repository/C2SC_MyJourney/txt_SubmitYourInitialEarnestMoney'), 'Submit your initial earnest money')

Mobile.verifyElementText(findTestObject('Object Repository/C2SC_MyJourney/txt_ScheduleInspection'), 'Schedule Inspection')

Mobile.verifyElementText(findTestObject('Object Repository/C2SC_MyJourney/txt_ReviewLoanEstimate'), 'Review Loan Estimate')

Mobile.verifyElementText(findTestObject('Object Repository/C2SC_MyJourney/txt_ReviewInspectionReport'), 'Review Inspection Report')

Mobile.verifyElementText(findTestObject('Object Repository/C2SC_MyJourney/txt_TitleCommitmentForm'), 'Title commitment form received')

Mobile.scrollToText('Review your Appraisal', FailureHandling.STOP_ON_FAILURE)

//Mobile.scrollToText('Final Closing Disclosure')
Mobile.verifyElementText(findTestObject('Object Repository/C2SC_MyJourney/txt_ReviewAppraisal'), 'Review your Appraisal')

Mobile.verifyElementText(findTestObject('Object Repository/C2SC_MyJourney/txt_SubmitYourEarnestMoney'), 'Submit your earnest money')

Mobile.verifyElementText(findTestObject('Object Repository/C2SC_MyJourney/txt_ObtainHomeownersInsurance'), 'Obtain Homeowners Insurance')

//Mobile.verifyElementText(findTestObject('Object Repository/C2SC_MyJourney/txt_Scheduleclosing'), 'Schedule closing')
//Mobile.scrollToText('Closing Documents Available')
//Mobile.verifyElementText(findTestObject('Object Repository/C2SC_MyJourney/txt_FinalClosingDisclosure'), 'Final Closing Disclosure')
//Mobile.verifyElementText(findTestObject('Object Repository/C2SC_MyJourney/txt_WireFunds'), 'Wire Funds for closing')
Mobile.closeApplication()

