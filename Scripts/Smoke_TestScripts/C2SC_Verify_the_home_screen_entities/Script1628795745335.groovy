import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementVisible(findTestObject('C2SC_AppLayout/icon_C2SC'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_AppLayout/icon_Notifications'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_AppLayout/txt_C2C'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_MyProfilePage/tab_Home'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_MyProfilePage/tab_MyDocs'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_MyProfilePage/tab_MyAgent'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_MyProfilePage/tab_MyTeam'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_MyProfilePage/tab_MyProfile'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

