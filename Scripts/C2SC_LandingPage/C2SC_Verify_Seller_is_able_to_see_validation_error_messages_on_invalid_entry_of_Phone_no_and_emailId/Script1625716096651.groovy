import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

Mobile.startApplication(GlobalVariable.sAppID, true)

WebUI.delay(10)

Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/input_Mobile'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_Mobile'), GlobalVariable.intWaitTime, 0)

Mobile.setText(findTestObject('C2SC_CommonObject/input_Mobile'), '1234', GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_CommonObject/txtLandingPage_ErrorPhoneMsg'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_LandingPage_Email'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/btn_LandingPage_Email'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_CommonObject/input_landing_Email'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_CommonObject/input_landing_Email'), 'test@test', GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime)

Mobile.verifyElementVisible(findTestObject('C2SC_CommonObject/txt_LandingPage_ErrorEmailMsg'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

