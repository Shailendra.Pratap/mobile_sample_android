import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil


Mobile.startApplication(GlobalVariable.sAppID, true)

WebUI.delay(10)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (Mobile.verifyElementExist(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
}

Mobile.verifyElementExist(findTestObject('C2SC_LoginScreen/lnk_Login'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_LoginScreen/lnk_Login'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_LoginScreen/input_Email'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_LoginScreen/input_Email'), GlobalVariable.intWaitTime, 0)

Mobile.setText(findTestObject('C2SC_LoginScreen/input_Email'), GlobalVariable.sEmailIdTitle, GlobalVariable.intWaitTime)

try {
	Mobile.hideKeyboard()
}
catch(Exception e) {
}

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_LoginScreen/input_Password'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_LoginScreen/input_Password'), GlobalVariable.intWaitTime, 0)

Mobile.setText(findTestObject('C2SC_LoginScreen/input_Password'), GlobalVariable.sPasswordTitle ,GlobalVariable.intWaitTime)

try {
	Mobile.hideKeyboard()
}
catch(Exception e) {
}

Mobile.waitForElementPresent(findTestObject('C2SC_LoginScreen/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_LoginScreen/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

if (Mobile.verifyElementExist(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
}

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('C2SC_HomePage/txt_My Team'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_MyTeamPage/txt_Mortgage'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyTeamPage/txt_Mortgage'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_GraSsnParameter/txt_graUserDetails_txt_web'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_GraSsnParameter/txt_graUserDetails_txt_web'), GlobalVariable.intWaitTime)

Mobile.delay(5)
if(AndroidDevice.equalsIgnoreCase('iOS')) {
Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_GraSsnParameter/web_Address'), GlobalVariable.intWaitTime)
}else {
	Mobile.tap(findTestObject('Object Repository/C2SC_GraSsnParameter/btn_Chrome'), GlobalVariable.intWaitTime)
	Mobile.tap(findTestObject('Object Repository/C2SC_GraSsnParameter/txt_Always'), GlobalVariable.intWaitTime)
	Mobile.delay(5)
	Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_GraSsnParameter/web_Address'), GlobalVariable.intWaitTime)
}
Mobile.closeApplication()