import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW

String AndroidDevice = GlobalVariable.sDeviceAndroid

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('C2SC_AddTeamMemberScreen/tab_MyTeam'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_AddTeamMemberScreen/btn_AddTeamMemberIcon'), GlobalVariable.intWaitTime)

Mobile.verifyElementText(findTestObject('C2SC_AddTeamMemberScreen/txt_AddTeamMemberHeader'), 'Who would you like to add?')

Mobile.tapAndHold(findTestObject('C2SC_AddTeamMemberScreen/input_firstName'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_AddTeamMemberScreen/input_firstName'), 'AnnZim', GlobalVariable.intWaitTime)

if (AndroidDevice.equals('iOS')) {
    Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.tapAndHold(findTestObject('C2SC_AddTeamMemberScreen/input_email'), GlobalVariable.intWaitTime, 0)

Mobile.setText(findTestObject('C2SC_AddTeamMemberScreen/input_email'), 'ann@a.com', GlobalVariable.intWaitTime)

if (AndroidDevice.equals('iOS')) {
    Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.tapAndHold(findTestObject('C2SC_AddTeamMemberScreen/input_phone'), GlobalVariable.intWaitTime, 0)

Mobile.setText(findTestObject('C2SC_AddTeamMemberScreen/input_phone'), '2323234434', GlobalVariable.intWaitTime)

if (AndroidDevice.equals('iOS')) {
    Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.tap(findTestObject('C2SC_HomePage/btn_Add'), GlobalVariable.intWaitTime)

not_run: Mobile.tap(findTestObject('C2SC_AddTeamMemberScreen/icon_PlusIconTeamMember'), GlobalVariable.intWaitTime)

Mobile.scrollToText('AnnZim', FailureHandling.STOP_ON_FAILURE)

//Mobile.getText(findTestObject('C2SC_AddTeamMemberScreen/txt_MyTeamName'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

