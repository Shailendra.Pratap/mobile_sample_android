import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('C2SC_MyProfilePage/tab_MyTeam'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyTeamPage/txt_Inspection'), GlobalVariable.intWaitTime)

Mobile.delay(5)

Mobile.waitForElementPresent(findTestObject('Object Repository/C2SC_AddTeamMemberScreen/icon_PlusIconTeamMember'), GlobalVariable.intWaitTime)

String CompanyName = 'Inspection Agency'

Mobile.tapAndHold(findTestObject('C2SC_MyTeamPage/input_CompanyName'), 5, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_MyTeamPage/input_CompanyName'), CompanyName, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), 'Jack', GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_MyTeamPage/input_AddTeamEmail'), 5,GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_MyTeamPage/input_AddTeamEmail'), 'test@test.com', GlobalVariable.intWaitTime)




String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('iOS')) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.tap(findTestObject('Object Repository/C2SC_AddTeamMemberScreen/icon_PlusIconTeamMember'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_MyTeamPage/txt_Please enter a valid phone number'), GlobalVariable.intWaitTime)

Mobile.closeApplication()



