import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_HomePageFlow'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('C2SC_HomePage/txt_My Team'), GlobalVariable.intWaitTime)

def validateMortgage = Mobile.getText(findTestObject('C2SC_MyTeamPage/txt_Mortgage'), GlobalVariable.intWaitTime)

Mobile.verifyEqual(validateMortgage, 'Mortgage')

Mobile.tap(findTestObject('C2SC_MyTeamPage/drp_Mortgage'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_MyTeamPage/img_GuaranteedRateAffinity'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_MyTeamPage/txt_Recommended'), GlobalVariable.intWaitTime)

def validateTitle = Mobile.getText(findTestObject('C2SC_MyTeamPage/txt_TitleClosing'), GlobalVariable.intWaitTime)

Mobile.verifyEqual(validateTitle, 'Title/Closing')

not_run: Mobile.tap(findTestObject('C2SC_MyTeamPage/drp_Title'), GlobalVariable.intWaitTime)

not_run: Mobile.verifyElementExist(findTestObject('C2SC_MyTeamPage/img_Title'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_MyTeamPage/txt_Recommended'), GlobalVariable.intWaitTime)

def validateHomeOwnersInsurance = Mobile.getText(findTestObject('C2SC_MyTeamPage/txt_HomeOwnersInsurance'), GlobalVariable.intWaitTime)

Mobile.verifyEqual(validateHomeOwnersInsurance, 'Home Owners Insurance')

Mobile.tap(findTestObject('C2SC_MyTeamPage/drp_HomeOwnerInsurance'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_MyTeamPage/img_RealogyInsuranceAgency'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_MyTeamPage/txt_Recommended'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

