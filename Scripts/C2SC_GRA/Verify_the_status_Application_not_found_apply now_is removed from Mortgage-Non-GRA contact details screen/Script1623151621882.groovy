import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin'), [:], FailureHandling.STOP_ON_FAILURE)

String AndroidDevice = GlobalVariable.sDeviceAndroid

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

if (AndroidDevice.equals('iOS')) {
	Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
	
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
	}

Mobile.tap(findTestObject('C2SC_HomePage/txt_My Team'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_MyTeamPage/txt_Mortgage'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyTeamPage/txt_Mortgage'), GlobalVariable.intWaitTime)

if (Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/btn_GRADetailDelete'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('C2SC_GraSsnParameter/btn_GRADetailDelete'), GlobalVariable.intWaitTime)

	Mobile.tap(findTestObject('C2SC_GraSsnParameter/btb_GRADelete'), GlobalVariable.intWaitTime)

	Mobile.delay(10)

	Mobile.tap(findTestObject('C2SC_GraSsnParameter/img_MortgageArrow'), GlobalVariable.intWaitTime)
}

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/Img_GRALogo'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/btn_Accept'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_GraSsnParameter/btn_Accept'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/Img_SSNScreenGRALogo'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_GraSsnParameter/Input_SSNField'), 5, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_GraSsnParameter/Input_SSNField'), GlobalVariable.ssnGRA, GlobalVariable.intWaitTime)

if (Mobile.verifyElementExist(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.tap(findTestObject('Object Repository/C2SC_GraSsnParameter/btn_Checkbox'), GlobalVariable.intWaitTime)

if(AndroidDevice.equalsIgnoreCase('Android')) {
	Mobile.scrollToText("Submit")
}

Mobile.tap(findTestObject('C2SC_MyTeamPage/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.delay(10)

Mobile.waitForElementPresent(findTestObject('C2SC_GraSsnParameter/Img_GRALogoLoanProviderScreen'), GlobalVariable.intWaitTime)

if (Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/txt_loanApplicationInProgress'), GlobalVariable.intWaitTime,
	FailureHandling.OPTIONAL)) {
	KeywordUtil.markPassed('Loan application in progress')
} else if (Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_GraSsnParameter/txt_loanApplicationNotFound'),
	GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
	KeywordUtil.markPassed('Loan application not found. Apply Now')
}

Mobile.waitForElementPresent(findTestObject('C2SC_GraSsnParameter/Img_closeIconLoanProviderScreen'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_GraSsnParameter/Img_closeIconLoanProviderScreen'), 5, GlobalVariable.intWaitTime)

Mobile.delay(5)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/Img_AcceptedGRALogo'), GlobalVariable.intWaitTime)

Mobile.closeApplication()
