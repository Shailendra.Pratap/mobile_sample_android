import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

//WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_HomePageFlow'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_EmailLogin'), [:], FailureHandling.STOP_ON_FAILURE)
String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('iOS')) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)
}
Mobile.tap(findTestObject('C2SC_AddTeamMemberScreen/tab_MyTeam'), GlobalVariable.intWaitTime)

Mobile.verifyElementText(findTestObject('C2SC_AddTeamMemberScreen/txt-MyTeam'), 'My Team')

Mobile.tap(findTestObject('C2SC_MyTeamPage/txt_Mortgage'), GlobalVariable.intWaitTime)

if (Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/btn_GRADetailDelete'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
    Mobile.tap(findTestObject('C2SC_GraSsnParameter/btn_GRADetailDelete'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_GraSsnParameter/btb_GRADelete'), GlobalVariable.intWaitTime)

    Mobile.delay(10)

    Mobile.tap(findTestObject('C2SC_GraSsnParameter/img_MortgageArrow'), GlobalVariable.intWaitTime)
}
if(Mobile.verifyElementExist(findTestObject('C2SC_MyTeamPage/btn_Edit'), GlobalVariable.intWaitTime,FailureHandling.OPTIONAL)) {
	Mobile.tap(findTestObject('C2SC_MyTeamPage/btn_Edit'), GlobalVariable.intWaitTime)
}
Mobile.verifyElementVisible(findTestObject('Object Repository/C2SC_MyTeamPage/txt_WhoWouldYouLikeToAdd'), GlobalVariable.intWaitTime,FailureHandling.OPTIONAL)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_CompanyName'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_CompanyName'),'TCS',GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime,FailureHandling.OPTIONAL)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_ContactName'), 'Ann Tester', GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime,FailureHandling.OPTIONAL)

Mobile.delay(GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_Email'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

String email=CustomKeywords.'com.Utilities.Utility.getRandomEmail'()

String getRandomEmailId=email.toLowerCase()

Mobile.setText(findTestObject('C2SC_AddTeamMemberScreen/input_email'),getRandomEmailId, GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime,FailureHandling.OPTIONAL)

Mobile.tapAndHold(findTestObject('Object Repository/C2SC_MyTeamPage/input_PhoneNumber'), GlobalVariable.intWaitTime, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('Object Repository/C2SC_MyTeamPage/input_PhoneNumber'), '1234567890', GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime,FailureHandling.OPTIONAL)

Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/btn_AddSPPlusIcon'), GlobalVariable.intWaitTime)

//Mobile.tap(findTestObject('C2SC_MyTeamPage/txt_Mortgage'), GlobalVariable.intWaitTime)

//Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/Img_GRALogo'), GlobalVariable.intWaitTime)
//
//Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/btn_Accept'), GlobalVariable.intWaitTime)
//
//Mobile.tap(findTestObject('C2SC_GraSsnParameter/btn_Accept'), GlobalVariable.intWaitTime)

//Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/Img_SSNScreenGRALogo'), GlobalVariable.intWaitTime)
//
//Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/txt_confirmSSN'), GlobalVariable.intWaitTime)
//
//Mobile.tapAndHold(findTestObject('C2SC_GraSsnParameter/Input_SSNField'), 5, GlobalVariable.intWaitTime)
//
//Mobile.setText(findTestObject('C2SC_GraSsnParameter/Input_SSNField'), '12', GlobalVariable.intWaitTime)
//
//Mobile.verifyElementText(findTestObject('C2SC_GraSsnParameter/Input_SSNField'), '')

Mobile.closeApplication()

