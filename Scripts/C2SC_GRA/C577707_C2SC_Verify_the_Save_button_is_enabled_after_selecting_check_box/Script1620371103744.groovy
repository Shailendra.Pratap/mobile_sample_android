import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_HomePageFlow'), [:], FailureHandling.STOP_ON_FAILURE)

String Device= GlobalVariable.sDeviceAndroid
String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('iOS')) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)
}
Mobile.tap(findTestObject('C2SC_AddTeamMemberScreen/tab_MyTeam'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyTeamPage/txt_Mortgage'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_MyTeamPage/img_GuaranteedRateAffinity'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_MyTeamPage/txt_Recommended'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyTeamPage/txt_Accept'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_MyTeamPage/img_Guaranteed'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('Object Repository/C2SC_GraSsnParameter/btn_Checkbox'), GlobalVariable.intWaitTime)

if(Device.equalsIgnoreCase('Android')) {
	Mobile.scrollToText("Submit")
}

 String s= Mobile.getAttribute(findTestObject('C2SC_MyTeamPage/btn_Submit'),'enabled' , GlobalVariable.intWaitTime)

if(s.equalsIgnoreCase("true"))
{
	KeywordUtil.markPassed("Submit button is enabled after clicking checkbox")
}else {
	KeywordUtil.markFailed("Submit button is not enabled after clicking checkbox")
}
