import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

WebUI.callTestCase(findTestCase('C2SC_Common_Testcase/C2SC_HomePageFlow'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)
String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('iOS')) {
	Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)
}
Mobile.tap(findTestObject('C2SC_HomePage/txt_My Team'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_MyTeamPage/txt_Mortgage'), GlobalVariable.intWaitTime)

//
Mobile.tap(findTestObject('C2SC_MyTeamPage/txt_Mortgage'), GlobalVariable.intWaitTime)
Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Accept'), GlobalVariable.intWaitTime)
Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Edit'), GlobalVariable.intWaitTime)
Mobile.tap(findTestObject('C2SC_MyTeamPage/txt_Mortgage'), GlobalVariable.intWaitTime)

//
Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/txt_HomeOwnersInsurance'), GlobalVariable.intWaitTime)
Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Accept'), GlobalVariable.intWaitTime)
Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_MyTeamPage/btn_Edit'), GlobalVariable.intWaitTime)
Mobile.tap(findTestObject('Object Repository/C2SC_MyTeamPage/txt_HomeOwnersInsurance'), GlobalVariable.intWaitTime)

Mobile.closeApplication()

