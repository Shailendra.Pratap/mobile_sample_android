import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil

Mobile.startApplication(GlobalVariable.sAppID, true)

WebUI.delay(10)

String AndroidDevice = GlobalVariable.sDeviceAndroid

if (AndroidDevice.equals('iOS')) {
    Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)
}

Mobile.verifyElementExist(findTestObject('C2SC_LoginScreen/lnk_Login'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_LoginScreen/lnk_Login'), GlobalVariable.intWaitTime)

Mobile.waitForElementPresent(findTestObject('C2SC_LoginScreen/input_Email'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_LoginScreen/input_Email'), GlobalVariable.intWaitTime, 0)

Mobile.setText(findTestObject('C2SC_LoginScreen/input_Email'), GlobalVariable.ssEmailidGRA, GlobalVariable.intWaitTime)

if (AndroidDevice.equals('iOS')) {
    Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.waitForElementPresent(findTestObject('C2SC_LoginScreen/input_Password'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_LoginScreen/input_Password'), GlobalVariable.intWaitTime, 0)

Mobile.setText(findTestObject('C2SC_LoginScreen/input_Password'), GlobalVariable.sPasswordGRA, GlobalVariable.intWaitTime)



/*if (AndroidDevice.equals('iOS')) {
    Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}*/
Mobile.hideKeyboard()

Mobile.waitForElementPresent(findTestObject('C2SC_LoginScreen/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_LoginScreen/btn_Submit'), GlobalVariable.intWaitTime)

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

if (AndroidDevice.equals('Android')) {
    try {
        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

        Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

        Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

        Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
    }
    catch (Exception e) {
    } 
}
if (AndroidDevice.equals('iOS')) {
Mobile.waitForElementPresent(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_CommonObject/btn_Allow'), GlobalVariable.intWaitTime)
}

Mobile.delay(GlobalVariable.intWaitTime, FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('C2SC_AddTeamMemberScreen/tab_MyTeam'), GlobalVariable.intWaitTime)

Mobile.verifyElementText(findTestObject('C2SC_AddTeamMemberScreen/txt-MyTeam'), 'My Team')

//Mobile.tap(findTestObject('C2SC_GraSsnParameter/img_MortgageArrow'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_MyTeamPage/txt_Mortgage'), GlobalVariable.intWaitTime)

if (Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/btn_GRADetailDelete'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
    Mobile.tap(findTestObject('C2SC_GraSsnParameter/btn_GRADetailDelete'), GlobalVariable.intWaitTime)

    Mobile.tap(findTestObject('C2SC_GraSsnParameter/btb_GRADelete'), GlobalVariable.intWaitTime)

    Mobile.delay(10)

    Mobile.tap(findTestObject('C2SC_GraSsnParameter/img_MortgageArrow'), GlobalVariable.intWaitTime)
}

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/Img_GRALogo'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/btn_Accept'), GlobalVariable.intWaitTime)

Mobile.tap(findTestObject('C2SC_GraSsnParameter/btn_Accept'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/Img_SSNScreenGRALogo'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/Img_closeIconSSN'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/txt_PropertyDetails'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/txt_ApplicationDetails'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/txt_PrimaryApplicantNote'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/txt_PrimaryEmail'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/txt_confirmSSN'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_GraSsnParameter/Input_SSNField'), 5, GlobalVariable.intWaitTime)

Mobile.setText(findTestObject('C2SC_GraSsnParameter/Input_SSNField'), GlobalVariable.ssnGRA, GlobalVariable.intWaitTime)

if (Mobile.verifyElementExist(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
    Mobile.tap(findTestObject('C2SC_CommonObject/btn_KeyboardDone'), GlobalVariable.intWaitTime)
}

Mobile.tap(findTestObject('Object Repository/C2SC_GraSsnParameter/btn_Checkbox'), GlobalVariable.intWaitTime)
Mobile.swipe(170, 640, 170, 490)
Mobile.tap(findTestObject('C2SC_GraSsnParameter/btn_Save'), GlobalVariable.intWaitTime)

Mobile.delay(10)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/Img_GRALogoLoanProviderScreen'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_GraSsnParameter/txt_ContactNameLoanProviderScreen'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/txt_EmailLoanProviderScreen'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/txt_PhoneLoanProviderScreen'), GlobalVariable.intWaitTime)

if (Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/txt_loanApplicationInProgress'), GlobalVariable.intWaitTime, 
    FailureHandling.OPTIONAL)) {
    KeywordUtil.markPassed('Loan application in progress')
} else if (Mobile.verifyElementExist(findTestObject('Object Repository/C2SC_GraSsnParameter/txt_loanApplicationNotFound'), 
    GlobalVariable.intWaitTime, FailureHandling.OPTIONAL)) {
    KeywordUtil.markPassed('Loan application not found. Apply Now')
}

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/btn_DeleteLoanOfficer'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/btn_EditLoanOfficer'), GlobalVariable.intWaitTime)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/Img_closeIconLoanProviderScreen'), GlobalVariable.intWaitTime)

Mobile.tapAndHold(findTestObject('C2SC_GraSsnParameter/Img_closeIconLoanProviderScreen'), 5, GlobalVariable.intWaitTime)

Mobile.delay(5)

Mobile.verifyElementExist(findTestObject('C2SC_GraSsnParameter/Img_AcceptedGRALogo'), GlobalVariable.intWaitTime)