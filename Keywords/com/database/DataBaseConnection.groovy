package com.database

import org.bson.Document
import com.kms.katalon.core.annotation.Keyword
import com.mongodb.BasicDBObject
import com.mongodb.DB
import com.mongodb.DBCollection
import com.mongodb.DBObject
import com.mongodb.MongoClient
import com.mongodb.MongoClientURI
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.bson.conversions.Bson

public class DataBaseConnection {

	@Keyword
	public String getDetailsFromDb(String sCollectionName, String sQueryColumnName,String sQueryColumnValue,String sGetColumnName){
		MongoClientURI uri = new MongoClientURI(GlobalVariable.dbServer)
		MongoClient mongoClient = new MongoClient(uri)
		DB db = mongoClient.getDB(GlobalVariable.dbName)

		String name = db.getName()
		println(name)

		DBCollection coll = db.getCollection(sCollectionName)
		println(coll)

		DBObject dbObject  = new BasicDBObject(sQueryColumnName, sQueryColumnValue)
		def resultObject   = coll.find(dbObject).next()
		String result =  resultObject.get(sGetColumnName)
		println result

		return result
	}

	@Keyword
	public String UpdateDetailsDb(String sCollectionName, String sQueryColumnName, String sQueryColumnValue) {
		MongoClientURI uri = new MongoClientURI(GlobalVariable.dbServer)
		MongoClient mongoClient = new MongoClient(uri)
		MongoDatabase database = mongoClient.getDatabase(GlobalVariable.dbName);
		MongoCollection<Document>collection = database.getCollection(sCollectionName);
		collection.updateOne(Filters.eq(sQueryColumnName, sQueryColumnValue), Updates.set("status", "Invited"));
	}

	@Keyword
	public String UpdateDetailsDbStatus(String sTransactionN, String stransColumnName, String stransColumnValue) {
		MongoClientURI uri = new MongoClientURI(GlobalVariable.dbServer)
		MongoClient mongoClient = new MongoClient(uri)
		MongoDatabase database = mongoClient.getDatabase(GlobalVariable.dbName);
		MongoCollection<Document>collection = database.getCollection(sTransactionN);
		collection.updateOne(Filters.eq(stransColumnName, stransColumnValue), Updates.set("status", "Invitation Triggered"));
	}

	@Keyword
	public String updateStartDateToDoDb(String sCollectionName, String sQueryColumnName, String sQueryColumnValue, String sQueryColumnName2, String sQueryColumnValue2) {
		Bson filterTransactionId = Filters.eq(sQueryColumnName, sQueryColumnValue)
		Bson filterReferenceId = Filters.eq(sQueryColumnName2, sQueryColumnValue2)
		MongoClientURI uri = new MongoClientURI(GlobalVariable.dbServer)
		MongoClient mongoClient = new MongoClient(uri)
		MongoDatabase database = mongoClient.getDatabase(GlobalVariable.dbName)
		MongoCollection<Document>collection = database.getCollection(sCollectionName)
		collection.updateOne(Filters.and(filterTransactionId, filterReferenceId), Updates.set("startDate", java.time.Clock.systemUTC().instant()))
	}
}
