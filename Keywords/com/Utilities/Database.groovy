package com.Utilities
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable



import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.testng.asserts.SoftAssert;

public class Database {
	public static ResultSet rs=null;
	static String Query=null;
	public static void executeQuery(String Q) throws Exception {
		Query = Q;
	}
	public static void main(String[] args) throws Exception {
		Database.executeQuery("select top 1 * from Billing.TRFCalculationSummary_view where TransactionKey=413590");
		System.out.println(getResultset(1));
	}

	static Connection connection = null;
	Statement stmt = null;
	SoftAssert sAssert= new SoftAssert();
	@Keyword
	public static void getDataBaseConnection() throws Exception {
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		String connectionUrl = "jdbc:sqlserver://" + GlobalVariable.dbServerName + ";databaseName="
		+ GlobalVariable.sDatabaseName + ";user=" + GlobalVariable.sSQLUserName + ";password="
		+ GlobalVariable.sSQLPassword;


		try {
			connection = DriverManager.getConnection(connectionUrl);
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
	}
	@Keyword
	public static String getResultset(String ColumnName) throws Exception{
		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}


		try {
			rs = stmt.executeQuery(Query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			while (rs.next()) {
				String testDate = rs.getString(ColumnName);


				return testDate;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	@Keyword
	public static String getResultset(int columnNumber) throws Exception{


		Statement stmt = null;
		try {
			stmt = connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}


		try {
			rs = stmt.executeQuery(Query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			while (rs.next()) {
				String testDate = rs.getString(columnNumber);

				return testDate;
			}
		} catch (SQLException e) {
			e.printStackTrace();
			//Log.fatal("Unable to get data from result set");
		}
		return null;
	}
}

