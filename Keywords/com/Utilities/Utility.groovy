package com.Utilities

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Properties
import java.util.Random

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import com.kms.katalon.core.webui.driver.DriverFactory as DF
import internal.GlobalVariable
import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.ui.ExpectedCondition
import org.openqa.selenium.support.ui.WebDriverWait
import com.Utilities.Log

public class Utility {

	Log log = new Log();
	//This function verify the file id downloaded or not and then delete that file
	@Keyword
	public Boolean verifyDownload(String exportFileName, String format) throws Exception {
		String survey_export = exportFileName;
		String home = System.getProperty("user.home");
		String filePath = home + File.separator + "Downloads";
		String fileName = survey_export + format;
		println(fileName)
		/*WebDriver driver = DF.getWebDriver();
		 final WebDriverWait wait = new WebDriverWait(driver, 180);*/
		try{
			final File dir = new File(filePath);


			for(int i= 1;i<=60;i++)
			{
				Thread.sleep(1000)
				File[] dirContents = dir.listFiles();
				for (File dirContent : dirContents) {
					if (dirContent.getName().equals(fileName)) {
						dirContent.delete();
						return true;

					}
				}
			}
		}catch(Exception e) {
			return false;
		}
		return false;
	}





	//This function get the windowHandles and verifies if the window is opened
	@Keyword
	public Boolean checkWindowTitle(String windowTitle) throws Exception {
		try
		{
			WebDriver driver = DF.getWebDriver();

			for (String shandle : driver.getWindowHandles()) {
				if (driver.switchTo().window(shandle).getTitle().contains(windowTitle) == true) {
					driver.switchTo().defaultContent();

					return true;
				} else {

					return false;
				}
			}


			return false;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());


		}
	}


	//This function returns a date in string format with offset of x days from current date
	@Keyword
	public String getFutureDate(int intDays) {
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		Calendar cal = Calendar.getInstance();
		try
		{
			cal.add(Calendar.DATE, intDays);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return dateFormat.format(cal.getTime());
	}

	//This function returns a date in string format with offset of x days from current date
	@Keyword
	public String getBeforeDate(int intDays){

		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		Calendar cal = Calendar.getInstance();
		try
		{
			cal.add(Calendar.DATE, -intDays);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return dateFormat.format(cal.getTime());
	}



	//This function gets the current date and return date in string format
	@Keyword
	public String getCurrentDate() {

		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
		Date date = new Date();
		String sDate;
		try
		{
			sDate = formatter.format(date);
		}

		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());
		}

		return sDate;
	}

	//This function gets No of length and return random string of that length
	@Keyword
	public String getSaltString(int intlength) {
		String ssaltStr = ""
		try
		{
			int leftLimit = 65; // letter 'A'
			int rightLimit = 90; // letter 'Z'
			StringBuilder ssalt = new StringBuilder();
			Random rnd = new Random();
			while (ssalt.length() < intlength) {
				int randomLimitedInt = leftLimit + (int) (rnd.nextFloat() * (rightLimit - leftLimit + 1));
				ssalt.append((char) randomLimitedInt);
			}
			ssaltStr = ssalt.toString();
		}

		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return ssaltStr;
	}

	//This function gets No of length and return random Email Id of that length
	@Keyword

	public  String getRandomEmail() {
		String ssaltStr = ""
		String ssaltInt= ""
		String sEmail= ""
		try
		{
			int leftLimit = 65; // letter 'A'
			int rightLimit = 90; // letter 'Z'
			StringBuilder ssalt = new StringBuilder();
			Random rnd = new Random();
			while (ssalt.length() < 5) {
				int randomLimitedInt = leftLimit + (int) (rnd.nextFloat() * (rightLimit - leftLimit + 1));
				ssalt.append((char) randomLimitedInt);
			}
			ssaltStr=ssalt.toString()

			int intleftLimit = 49; // number '1'
			int intrightLimit = 57; // number '9'
			StringBuilder ssalt1 = new StringBuilder();
			Random rnd1 = new Random();
			for (int i = 0; i < 3; i++)
			{
				ssalt1.append((char) (intleftLimit + rnd1.nextDouble() * (intrightLimit - intleftLimit)));
			}
			ssaltInt = ssalt1.toString()

			sEmail=ssaltStr+ssaltInt+"@test.com"
		}

		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return sEmail;
	}

	//This function gets No of length and return random Number of that length
	@Keyword

	public  String getSaltNumber(int length) {
		StringBuilder ssalt = new StringBuilder();
		try
		{


			int intleftLimit = 49; // number '1'
			int intrightLimit = 57; // number '9'
			Random rnd = new Random();
			for (int i = 0; i < length; i++)
			{
				ssalt.append((char) (intleftLimit + rnd.nextDouble() * (intrightLimit - intleftLimit)));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return ssalt.toString();
	}
	@Keyword
	public void putData(String sPropertyName, String sValue) {
		Properties prop = new Properties();
		OutputStream output = null;
		def sPropPath = RunConfiguration.getProjectDir() + "\\Properties\\DataValues.properties"
		try
		{
			File filePropPath = new File(sPropPath);
			if(!filePropPath.exists())
			{
				filePropPath.createNewFile();


			}
			output = new FileOutputStream(sPropPath);
			prop.put(sPropertyName, sValue);
			prop.store(output, null);
			output.close();

		}

		catch(Exception e)
		{
			e.printStackTrace();
			log.error(e.getMessage());
		}
	}

	@Keyword
	public String getData(String sPropertyName) throws Exception

	{
		String sValue = "";
		try {
			def sPropPath = RunConfiguration.getProjectDir() + "\\Properties\\DataValues.properties"
			FileInputStream f = new FileInputStream(sPropPath)
			Properties p = new Properties();
			p.load(f);
			sValue = p.getProperty(sPropertyName);
		}

		catch (Exception e)

		{
			e.printStackTrace();
			log.error(e.getMessage());
		}

		return sValue;
	}

	@Keyword
	public boolean isFilePresent(String sDownloadPath, String sFileName) {
		try {
			final File dir = new File(sDownloadPath);
			final File[] dirContents = dir.listFiles();
			for (final File dirContent : dirContents) {
				if (dirContent.getName().equals(sFileName)) {
					dirContent.delete();
					return true;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		return false;
	}


}


