package frameworkFunctions

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.constants.GlobalStringConstants
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable
import groovy.sql.Sql
import java.sql.*
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataExtraction {

	public static Connection connection = null;

	@Keyword
	def connectDB(){
		String url = "jdbc:sqlserver://" + GlobalVariable.dbServer + ";databaseName=" + GlobalVariable.dbName + ";user=" + GlobalVariable.dbUsername + ";password=" + GlobalVariable.dbPassword

		if(connection != null && !connection.isClosed()){
			connection.close()
			print "connection closed"
		}
		connection = DriverManager.getConnection(url)
		print connection
		return connection
	}


	@Keyword
	public ResultSet executeQuery(String queryString) {
		try{
			Statement stm = connection.createStatement()
			ResultSet rs = stm.executeQuery(queryString)
			return rs
		}catch(Exception e){}
	}



	@Keyword
	public HashMap<String,String> getDBValues(String sdbColumnNames,String sdbQuery) {
		HashMap<String, String> hmDatabaseValues = new HashMap<String, String>();

		//Connect to the database
		connectDB()


		List<String> lstColumnNames = Arrays.asList(sdbColumnNames.split(";"));
		//println(lstColumnNames)
		String squeryString = sdbQuery
		ResultSet rs =executeQuery(squeryString)

		ResultSetMetaData rsmd = rs.getMetaData();
		int intColumnCount= rsmd.getColumnCount();
		//println(intColumnCount)
		//Getting value for each column


		while(rs.next())
		{
			for (int i = 1; i<=intColumnCount ; i++)
			{

				String sColumnName = rsmd.getColumnName(i);
				String sColumnValue = rs.getObject(sColumnName)
				//println(lstColumnNames[i-1])
				hmDatabaseValues.put(lstColumnNames[i-1],sColumnValue)
				//println(lstColumnNames)
				//println(hmDatabaseValues)
			}
		}


		return hmDatabaseValues


	}


	@Keyword
	public HashMap<String,String> getDBValuesList(String sdbColumnNames,String sdbQuery) {
		HashMap<String, String> hmDatabaseValues = new HashMap<String, String>();

		//Connect to the database
		connectDB()



		//Getting value for each column
		String sColumnName = sdbColumnNames
		String squeryString = sdbQuery
		print 'hmDatabaseValues'
		//	println(squeryString)
		ResultSet rs =executeQuery(squeryString)


		while (rs.next())
		{
			sColumnName = sdbColumnNames
			String sColumnValue = rs.getString(sColumnName)

			int intSize = hmDatabaseValues.size();
			String sSize = Integer.toString(intSize);
			sColumnName = sColumnName + "_" + sSize;

			hmDatabaseValues.put(sColumnName,sColumnValue)
			println(sSize)

		}
		println(hmDatabaseValues)

		return hmDatabaseValues
	}
	@Keyword
	def closeDatabaseConnection() {
		if(connection != null && !connection.isClosed()){
			connection.close()
		}
		connection = null
	}

	@Keyword
	def execute(String queryString) {
		Statement stm = connection.createStatement()
		boolean result = stm.execute(queryString)
		return result
	}


	@Keyword

	def getValue(ResultSet rs , String sValueReqd) {
		String sActualValue
		//Print results from select statement
		while (rs.next()) {
			sActualValue = rs.getString(sValueReqd)
			System.out.println(sActualValue)
		}

		return sActualValue
	}
}
