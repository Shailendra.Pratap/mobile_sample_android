<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>btn_TranscationCompleted</name>
   <tag></tag>
   <elementGuidId>633e812c-33f8-49a9-af55-b7f704760da9</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>com.realogy.C2CShining:id/transaction_complete_button</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.ToggleButton</value>
   </webElementProperties>
   <locator>//*[@resource-id = 'com.realogy.C2CShining:id/transaction_complete_button' and @class = 'android.widget.ToggleButton']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
