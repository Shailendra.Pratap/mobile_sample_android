<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>bar_StarRating</name>
   <tag></tag>
   <elementGuidId>3639e005-0053-4b1f-b476-d689172ba008</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>com.realogy.C2CShining:id/satisfied_rating_bar</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.RatingBar</value>
   </webElementProperties>
   <locator>//*[@resource-id = 'com.realogy.C2CShining:id/satisfied_rating_bar' and @class = 'android.widget.RatingBar']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
