<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_WhatWentWrong</name>
   <tag></tag>
   <elementGuidId>8ca1cac2-f980-4fd5-9080-dd9975113373</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>What went wrong?</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>com.realogy.C2CShining:id/tv_what_went_wrong_text</value>
   </webElementProperties>
   <locator>//*[(@text = 'What went wrong?' or . = 'What went wrong?') and @resource-id = 'com.realogy.C2CShining:id/tv_what_went_wrong_text']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
