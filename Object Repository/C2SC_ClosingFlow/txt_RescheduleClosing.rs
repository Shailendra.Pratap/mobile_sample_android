<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description>XCUIElementTypeStaticText</description>
   <name>txt_RescheduleClosing</name>
   <tag></tag>
   <elementGuidId>ff2de6a8-b883-49df-b2d8-0786cdb9d70e</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>XCUIElementTypeStaticText</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Schedule_txt_title</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Reschedule Closing</value>
   </webElementProperties>
   <locator>//*[@type = 'XCUIElementTypeStaticText' and @name = 'Schedule_txt_title'] | //*[ (@text = 'Reschedule Closing' or . = 'Reschedule Closing')] </locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
