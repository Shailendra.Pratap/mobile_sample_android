<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_ClosingDate</name>
   <tag></tag>
   <elementGuidId>f4debecf-6b2b-417e-9b0e-2f056ae443cb</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Closing Date:</value>
   </webElementProperties>
   <locator>//*[contains(@text,'Closing Date:')]</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
