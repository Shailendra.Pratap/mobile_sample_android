<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_Congratulations on listing your home with Coldwell Banker</name>
   <tag></tag>
   <elementGuidId>fc314e26-e88b-45df-9785-8ce460a7457f</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Congratulations on listing your home with Coldwell Banker. Your Listing Agreement will be in MyDocs for your reference</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>com.realogy.C2CShining:id/tvToDoFormat</value>
   </webElementProperties>
   <locator>//*[(@text = 'Congratulations on listing your home with Coldwell Banker. Your Listing Agreement will be in MyDocs for your reference' or . = 'Congratulations on listing your home with Coldwell Banker. Your Listing Agreement will be in MyDocs for your reference') and @resource-id = 'com.realogy.C2CShining:id/tvToDoFormat'] | //*[@type = 'XCUIElementTypeStaticText' and @value = 'Congratulations on listing your home with Coldwell Banker. Your Listing Agreement will be in MyDocs for your reference']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
