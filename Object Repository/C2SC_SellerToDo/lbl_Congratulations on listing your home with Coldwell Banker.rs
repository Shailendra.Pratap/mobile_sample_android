<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>lbl_Congratulations on listing your home with Coldwell Banker</name>
   <tag></tag>
   <elementGuidId>34b5a90a-b18c-4aaa-817f-0f37e9cd8d51</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Congratulations on listing your home with Coldwell Banker!</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>com.realogy.C2CShining:id/tvToDoTitle</value>
   </webElementProperties>
   <locator>//*[(@text = 'Congratulations on listing your home with Coldwell Banker!' or . = 'Congratulations on listing your home with Coldwell Banker!') and @resource-id = 'com.realogy.C2CShining:id/tvToDoTitle'] | //*[@type = 'XCUIElementTypeStaticText' and @label = 'Congratulations on listing your home with Coldwell Banker!' and @name = 'todo_lbl_title']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
