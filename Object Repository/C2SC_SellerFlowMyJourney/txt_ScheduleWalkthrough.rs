<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_ScheduleWalkthrough</name>
   <tag></tag>
   <elementGuidId>70af3563-8045-43b8-a089-2ce9f33079a8</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>XCUIElementTypeStaticText</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Schedule Walkthrough</value>
   </webElementProperties>
   <locator>//*[@type = 'XCUIElementTypeStaticText' and @name = 'Schedule Walkthrough'] | //*[@text = 'Schedule Walkthrough' and @resource-id = 'com.realogy.homeplace.qa:id/my_journey_task_title']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
