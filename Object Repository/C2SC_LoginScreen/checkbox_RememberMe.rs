<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>checkbox_RememberMe</name>
   <tag></tag>
   <elementGuidId>ae822db0-3c7e-4804-b71d-a3b097f0aecf</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>XCUIElementTypeButton</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>login_btnRememberMe</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>label</name>
      <type>Main</type>
      <value>Unchecked GreyUnchecked Grey</value>
   </webElementProperties>
   <locator>//*[@type = 'XCUIElementTypeButton' and @name = 'login_btnRememberMe'] | //*[@text = 'Remember Me' and @class = 'android.widget.CheckBox' and @resource-id = 'com.realogy.C2CShining:id/saveLoginCheckBox']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
