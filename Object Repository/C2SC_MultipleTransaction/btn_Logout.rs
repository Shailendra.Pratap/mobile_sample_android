<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>btn_Logout</name>
   <tag></tag>
   <elementGuidId>31623811-6efc-4ea3-8a21-0f3298aa9c2a</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>com.realogy.homeplace.qa:id/tv_profile_logout</value>
   </webElementProperties>
   <locator>//*[@resource-id = 'com.realogy.homeplace.qa:id/tv_profile_logout'] | //*[@name = 'Logout']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
