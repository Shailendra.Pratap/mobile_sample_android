<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_Name</name>
   <tag></tag>
   <elementGuidId>5f67cd57-72ac-4da7-8458-2ea5ff8fe48e</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>com.realogy.homeplace.qa:id/tv_profile_name</value>
   </webElementProperties>
   <locator>//*[@resource-id = 'com.realogy.homeplace.qa:id/tv_profile_name']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
