<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>tab_todo</name>
   <tag></tag>
   <elementGuidId>9a224943-c005-45db-b103-1d5e471662e9</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>com.realogy.homeplace.qa:id/tab_title</value>
   </webElementProperties>
   <locator>//*[@resource-id = 'com.realogy.homeplace.qa:id/tab_title'] | // *[@name = 'To Do']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
