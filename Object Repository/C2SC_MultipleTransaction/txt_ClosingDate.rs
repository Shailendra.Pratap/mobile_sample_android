<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>txt_ClosingDate</name>
   <tag></tag>
   <elementGuidId>45cc23f8-73ce-4d25-878c-e733ce7c703c</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>resource-id</name>
      <type>Main</type>
      <value>com.realogy.homeplace.qa:id/tv_closing_date</value>
   </webElementProperties>
   <locator>//*[@resource-id = 'com.realogy.homeplace.qa:id/tv_closing_date'] | //*[@name = 'Closing Date:']</locator>
   <locatorStrategy>XPATH</locatorStrategy>
</MobileElementEntity>
